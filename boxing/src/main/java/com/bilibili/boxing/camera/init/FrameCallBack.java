/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.camera.init;

import ohos.media.camera.device.Camera;
import ohos.media.camera.device.FrameConfig;
import ohos.media.camera.device.FrameResult;
import ohos.media.camera.device.FrameStateCallback;

/**
 * Frame Processing Callback
 */
public class FrameCallBack extends FrameStateCallback {

    /**
     * Trigger callback when frame capture starts
     *
     * @param camera      Camera object
     * @param frameConfig Frame Configuration Information
     * @param frameNumber Frame number
     * @param timestamp   Frame capture timestamp
     */
    @Override
    public void onFrameStarted(Camera camera, FrameConfig frameConfig, long frameNumber, long timestamp) {
    }

    /**
     * Called when frame capture is complete and all results are available
     *
     * @param camera      Camera object
     * @param frameConfig Frame Configuration Information
     * @param frameResult Frame capture results
     */
    @Override
    public void onFrameFinished(Camera camera, FrameConfig frameConfig, FrameResult frameResult) {
    }

    /**
     * Called when there are partial results during frame capture
     *
     * @param camera      Camera object
     * @param frameConfig Frame Configuration Information
     * @param frameResult Frame capture results
     */
    @Override
    public void onFrameProgressed(Camera camera, FrameConfig frameConfig, FrameResult frameResult) {
    }

    /**
     * Call when an error occurs during frame capture
     *
     * @param camera      Camera object
     * @param frameConfig Frame Configuration Information
     * @param errorCode   Error code
     * @param frameResult Capture results
     */
    @Override
    public void onFrameError(Camera camera, FrameConfig frameConfig, int errorCode, FrameResult frameResult) {
    }

    /**
     * Called when the trigger is started
     *
     * @param camera           Camera object
     * @param captureTriggerId ID of triggers
     * @param firstFrameNumber First frame number
     */
    @Override
    public void onCaptureTriggerStarted(Camera camera, int captureTriggerId, long firstFrameNumber) {
    }

    /**
     * Called when the triggered frame capture action is complete
     *
     * @param camera           Camera object
     * @param captureTriggerId ID of triggers
     * @param lastFrameNumber  Last frame number -1 means no frame is captured
     */
    @Override
    public void onCaptureTriggerFinished(Camera camera, int captureTriggerId, long lastFrameNumber) {
    }

    /**
     * Called when the triggered frame capture action stops early
     *
     * @param camera           Camera object
     * @param captureTriggerId ID of triggers
     */
    @Override
    public void onCaptureTriggerInterrupted(Camera camera, int captureTriggerId) {
    }

}
