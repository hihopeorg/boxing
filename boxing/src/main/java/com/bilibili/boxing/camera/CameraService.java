/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.camera;

import com.bilibili.boxing.ResourceTable;
import com.bilibili.boxing.camera.init.CameraInit;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.utils.LayoutAlignment;
import ohos.media.image.common.ImageFormat;

/**
 * Camera business layer
 */
public class CameraService {

    /**
     * abilitySlice object
     */
    public static AbilitySlice abilitySlice;

    /**
     * Camera Initialization
     *
     * @param abilitySlice abilitySlice object
     */
    public static synchronized void initCameraService(AbilitySlice abilitySlice) {
        CameraService cameraService = new CameraService();
        CameraService.abilitySlice = abilitySlice;
        // Initialize camera configuration
        CameraInfoConfig.initCameraConfig(abilitySlice.getContext());
        // Create a camera
        cameraService.createCamera();
    }

    /**
     * Create a camera
     */
    private void createCamera() {
        // Gets the component object
        StackLayout stackLayout = (StackLayout) abilitySlice.findComponentById(ResourceTable.Id_stack_layout);
        SurfaceProvider surfaceProvider = new SurfaceProvider(abilitySlice.getContext());
        // At the top of the AGP container assembly
        surfaceProvider.pinToZTop(true);
        // Setting styles
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig();
        layoutConfig.alignment = LayoutAlignment.CENTER;
        layoutConfig.setMarginBottom(55);
        //Set camera length and width according to scale
        layoutConfig.width = CameraInfoConfig.cameraSize.width;
        layoutConfig.height = CameraInfoConfig.cameraSize.height;
        surfaceProvider.setLayoutConfig(layoutConfig);
        // Add the component to the container
        stackLayout.addComponent(surfaceProvider);
        // Gets SurfaceOps object
        SurfaceOps surfaceOps = surfaceProvider.getSurfaceOps().get();
        // Set Pixel Format
        surfaceOps.setFormat(ImageFormat.JPEG);
        // The settings screen opens
        surfaceOps.setKeepScreenOn(true);
        // Add a callback
        surfaceOps.addCallback(new SurfaceOpsCallBack());
        // Add callback photo button add click event
        Button pictureButton = (Button) abilitySlice.findComponentById(ResourceTable.Id_picture_button);
        ohos.agp.components.Image image = (ohos.agp.components.Image) abilitySlice.findComponentById(ResourceTable.Id_image);
        pictureButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                CameraInit.capture(image);
            }
        });

    }

    /**
     * surfaceOps Callback Implementation Class
     */
    class SurfaceOpsCallBack implements SurfaceOps.Callback {

        @Override
        public void surfaceCreated(SurfaceOps surfaceOps) {
            //Gets surface object
            Surface surface = surfaceOps.getSurface();
            CameraInit cameraInit = new CameraInit(abilitySlice, abilitySlice.getContext(), surface, surfaceOps);
            // Create a camera object
            boolean state = cameraInit.createCamera();
            if (state) { // create camera success
            } else {
            }
        }

        @Override
        public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {

        }

        @Override
        public void surfaceDestroyed(SurfaceOps surfaceOps) {

        }
    }

}
