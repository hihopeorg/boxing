/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.camera.init;

import com.bilibili.boxing.camera.CameraService;
import ohos.aafwk.content.Intent;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.media.image.Image;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageService implements Runnable {

    private final Image myImage;
    private final File myFile;
    ohos.agp.components.Image mImage;

    ImageService(Image image, File file, ohos.agp.components.Image images) {
        myImage = image;
        myFile = file;
        mImage = images;
    }

    @Override
    public void run() {
        Image.Component component = myImage.getComponent(ImageFormat.ComponentType.JPEG);
        byte[] bytes = new byte[component.remaining()];
        component.read(bytes);
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(myFile);
            output.write(bytes); // Writing Image Data

            TaskDispatcher uiTaskDispatcher = CameraService.abilitySlice.getUITaskDispatcher();
            uiTaskDispatcher.asyncDispatch(() -> {
                Intent intent = new Intent();
                //Execute UI update operations
                ImageSource.DecodingOptions checkOptions = new ImageSource.DecodingOptions();
                ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
                ImageSource imageSource = ImageSource.create(myFile, srcOpts);
                PixelMap pixelMap = imageSource.createPixelmap(checkOptions);
                mCallback.onFinish(myFile);
            });
        } catch (IOException e) {
        } finally {
            myImage.release();
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private Callback mCallback;

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        void onFinish(File file);

        void onError();
    }
}
