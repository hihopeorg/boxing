/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.camera.util;

import java.util.List;
import java.util.Map;

public class EmptyUtil {

    private EmptyUtil() {
    }

    public static Boolean isEmpty(String str) {
        if (str == null || str.equals("")) {
            return true;
        }
        return false;
    }

    public static Boolean isEmpty(List list) {
        if (list == null || list.size() == 0) {
            return true;
        }
        return false;
    }

    public static Boolean isEmpty(String[] strs) {
        if (strs == null || strs.length == 0) {
            return true;
        }
        return false;
    }

    public static Boolean isEmpty(Map map) {
        if (map == null || map.size() == 0) {
            return true;
        }
        return false;
    }

    public static Boolean isEmpty(Integer i) {
        if (i == null) {
            return true;
        }
        return false;
    }
}
