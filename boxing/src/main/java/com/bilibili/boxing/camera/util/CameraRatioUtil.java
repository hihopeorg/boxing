/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.camera.util;

import ohos.app.Context;
import ohos.media.image.common.Size;

import java.util.List;

/**
 * Camera Scale Calculation Tools
 */
public class CameraRatioUtil {

    /**
     * Full screen
     */
    public static final Integer FULL_SCREEN = 1;

    /**
     * 4：3
     */
    public static final Integer FOUR_TO_THREE = 2;

    /**
     * 1：1
     */
    public static final Integer ONE_TO_ONE = 3;


    /**
     * Gets the width value of the camera
     *
     * @param context
     * @param ratioType
     * @return
     */
    public static Size getCameraWidth(Context context, Integer ratioType) {
        Size size = new Size();
        // Gets the screen width pixel value
        Integer width = ScreenPixelsUtil.getDisplayWidthInPx(context);
        size.width = width;
        if (ratioType.equals(FULL_SCREEN)) {
            // If full screen, return to screen height directly
            size.height = ScreenPixelsUtil.getDisplayHeightInPX(context);
            return size;
        }
        if (ratioType.equals(FOUR_TO_THREE)) {
            Double four = 4.0;
            Double three = 3.0;
            // 4:3
            Double height = width * (four / three);
            size.height = height.intValue();
            return size;
        }
        if (ratioType.equals(ONE_TO_ONE)) {
            // If it's 1:1, return the width value directly
            size.height = width;
            return size;
        }
        return null;
    }

    /**
     * The closest preview size to the aspect ratio is obtained by comparison (if the same size is preferred)
     *
     * @param isPortrait    Is it vertical
     * @param surfaceWidth  Original width to be contrasted
     * @param surfaceHeight Original height to be contrasted
     * @param preSizeList   List of preview dimensions to be compared
     * @return Get the closest size to the original width and height ratio
     */
    public static Size getOptimalPreviewSize(boolean isPortrait, List<Size> preSizeList, int surfaceWidth, int surfaceHeight) {
        int reqTmpWidth;
        int reqTmpHeight;
        // When the screen is vertical, you need to change the width of the high value to ensure that the width is greater than the height
        if (isPortrait) {
            reqTmpWidth = surfaceHeight;
            reqTmpHeight = surfaceWidth;
        } else {
            reqTmpWidth = surfaceWidth;
            reqTmpHeight = surfaceHeight;
        }
        //Let's find out if the preview has the same width and height as the surfaceview
        for (Size size : preSizeList) {
            if ((size.width == reqTmpWidth) && (size.height == reqTmpHeight)) {
                return size;
            }
        }
        Size retSize = new Size();
        // Get the closest size to the incoming aspect ratio
        float reqRatio = ((float) reqTmpWidth) / ((float) reqTmpHeight);
        float curRatio, deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        for (Size size : preSizeList) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }
        return retSize;
    }

}
