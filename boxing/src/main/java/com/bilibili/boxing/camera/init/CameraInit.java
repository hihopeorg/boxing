/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.camera.init;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.camera.CameraInfoConfig;
import com.bilibili.boxing.camera.util.CameraRatioUtil;
import com.bilibili.boxing.camera.util.EmptyUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.camera.CameraKit;
import ohos.media.camera.device.*;
import ohos.media.image.Image;
import ohos.media.image.ImageReceiver;
import ohos.media.image.common.ImageFormat;

import java.io.File;
import java.util.UUID;

/**
 * Camera Initialization Class
 */
public class CameraInit implements ImageService.Callback {

    private static final HiLogLabel LOG = new HiLogLabel(HiLog.LOG_APP, 00201, CameraInit.class.getName());

    /**
     * Camera Runner Thread Name
     */
    private static final String EVENT_RUNNER_NAME = "LocalCamera";

    private static final String FRAME_RUNNER_NAME = "FrameCamera";

    /**
     * cameraKit Object
     */
    private static CameraKit cameraKit;

    /**
     * Preview surface
     */
    private Surface previewSurface;

    /**
     * Image frame data receiving and processing object
     */
    private static ImageReceiver imageReceiver;

    /**
     * Camera attributes
     */
    private CameraConfig.Builder cameraConfigBuilder;

    /**
     * Preview template
     */
    private static FrameConfig.Builder frameConfigBuilder;

    private AbilitySlice mAbilitySlice;
    private Context context;

    private static Camera myCamera;

    private SurfaceOps surfaceOps;

    public CameraInit(AbilitySlice slice, Context context, Surface surface, SurfaceOps surfaceOps) {
        this.mAbilitySlice = slice;
        this.context = context;
        this.previewSurface = surface;
        this.surfaceOps = surfaceOps;
    }

    /**
     * Create a camera
     *
     * @return
     */
    public boolean createCamera() {
        // Create cameraKit object
        boolean createKitState = this.createCameraKit(context);
        // Get the Camera List
        String[] cameraIds = this.getCameraIds();
        if (!createKitState || EmptyUtil.isEmpty(cameraIds)) {
            // Create failure
            return false;
        }
        // Gets the cameraAbility object based on the front or rear camera used
        CameraAbility cameraAbility = cameraKit.getCameraAbility(cameraIds[CameraInfoConfig.cameraType]);
        //Get a photo-enabled resolution list
        CameraInfoConfig.pictureSizes = cameraAbility.getSupportedSizes(ImageFormat.JPEG);
        // Optimal resolution
        CameraInfoConfig.pictureOptimumSize = CameraRatioUtil.getOptimalPreviewSize(true, CameraInfoConfig.pictureSizes, CameraInfoConfig.cameraSize.width, CameraInfoConfig.cameraSize.height);
        // Settings Preview surface Properties
        this.setSurface(previewSurface);
        // Initialize image receiver object
        this.initImageReceiver();
        // Creates a camera object that calls the callback method in the cameraStateCallback. For example
        cameraKit.createCamera(cameraIds[CameraInfoConfig.cameraType], new CameraCallback(), new EventHandler(EventRunner.create(EVENT_RUNNER_NAME)));
        return true;
    }

    /**
     * Create CameraKit object
     *
     * @param context
     * @return
     */
    private boolean createCameraKit(Context context) {
        //Gets CameraKit instance
        cameraKit = CameraKit.getInstance(context);
        if (cameraKit == null) {
            return false;
        }
        return true;
    }

    /**
     * Gets the camera id array
     *
     * @return
     */
    private String[] getCameraIds() {
        String[] ids = null;
        try {
            ids = cameraKit.getCameraIds();
        } catch (IllegalStateException e) {
        }
        return ids;
    }

    /**
     * surface set
     *
     * @param surface
     */
    private void setSurface(Surface surface) {
        // Setting raw image data format and size
        surface.showRawImage(
                new byte[Surface.PixelFormat.PIXEL_FORMAT_YCBCR_422_I.value() + CameraInfoConfig.pictureOptimumSize.width + CameraInfoConfig.pictureOptimumSize.height],
                Surface.PixelFormat.PIXEL_FORMAT_YCBCR_422_I,
                CameraInfoConfig.pictureOptimumSize.width,
                CameraInfoConfig.pictureOptimumSize.height);
        // Synchronize drawing calls on canvas objects for rendering
        surface.syncCanvasDrawCalls();
    }

    private void initImageReceiver() {
        //  To create a ImageReceiver object, note that the width of the create function is greater than the maximum number of images supported by height 5, please set according to the actual
        imageReceiver = ImageReceiver.create(Math.max(CameraInfoConfig.pictureOptimumSize.width, CameraInfoConfig.pictureOptimumSize.height),
                Math.min(CameraInfoConfig.pictureOptimumSize.width, CameraInfoConfig.pictureOptimumSize.height),
                ImageFormat.JPEG, 5);
        imageReceiver.setImageArrivalListener(imageArrivalListener);
    }

    static ohos.agp.components.Image mImage;

    public static void capture(ohos.agp.components.Image image) {
        mImage = image;
        //Get the Photo Configuration Template
        frameConfigBuilder = myCamera.getFrameConfigBuilder(Camera.FrameConfigType.FRAME_CONFIG_PICTURE);
        //Configuration photo Surface
        frameConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
        // Configure other parameters
        frameConfigBuilder.setImageRotation(90);
        try {
            // Start single frame capture (photo)
            myCamera.triggerSingleCapture(frameConfigBuilder.build());
        } catch (IllegalArgumentException e) {
            HiLog.error(LOG, "Argument Exception");
        } catch (IllegalStateException e) {
            HiLog.error(LOG, "State Exception");
        }
    }

    /**
     * Single frame capture generates image callback Listener
     */
    private final ImageReceiver.IImageArrivalListener imageArrivalListener = new ImageReceiver.IImageArrivalListener() {
        @Override
        public void onImageArrival(ImageReceiver imageReceiver) {
            // Set Picture Name
            StringBuffer fileName = new StringBuffer("picture_");
            fileName.append(UUID.randomUUID()).append(".jpg");
            File dirFile = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File myFile = new File(dirFile, fileName.toString());
            // Save the picture
            Image image = imageReceiver.readNextImage();
            ImageService imageService = new ImageService(image, myFile, mImage);
            imageService.setCallback(CameraInit.this);
            EventHandler eventHandler = new EventHandler(EventRunner.create("CameraCb"));
            eventHandler.postTask(imageService);
        }
    };

    @Override
    public void onFinish(File file) {
        Intent intent = new Intent();
        intent.setParam(Boxing.EXTRA_RESULT_CAMERA, file);
        mAbilitySlice.getAbility().setResult(Boxing.CAMERA_RESULT_CODE, intent);
        mAbilitySlice.getAbility().terminateAbility();
    }

    @Override
    public void onError() {

    }

    /**
     * Camera creates callback inner class
     */
    class CameraCallback extends CameraStateCallback {

        /**
         * Camera creation success callback
         *
         * @param camera
         */
        @Override
        public void onCreated(Camera camera) {
            myCamera = camera;
            // Camera configuration
            cameraConfigBuilder = camera.getCameraConfigBuilder();
            // Set Preview surface
            cameraConfigBuilder.addSurface(previewSurface);
            // Set up photo surface
            cameraConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
            // callback for configuration frame results
            FrameCallBack frameStateCallBack = new FrameCallBack();
            EventHandler eventHandler = new EventHandler(EventRunner.create(FRAME_RUNNER_NAME));
            cameraConfigBuilder.setFrameStateCallback(frameStateCallBack, eventHandler);
            try {
                // Camera device configuration, trigger onConfigured callback
                camera.configure(cameraConfigBuilder.build());
            } catch (IllegalArgumentException e) {

            } catch (IllegalStateException e) {

            }
        }

        /**
         * Camera Configuration Callback
         *
         * @param camera
         */
        @Override
        public void onConfigured(Camera camera) {
            // Gets a preview template
            frameConfigBuilder = camera.getFrameConfigBuilder(Camera.FrameConfigType.FRAME_CONFIG_PREVIEW);
            // Configuration Preview surface
            frameConfigBuilder.addSurface(previewSurface);
            //Set magnification
//            frameConfigBuilder.setZoom(1.2f);

            FrameConfig previewFrameConfig = frameConfigBuilder.build();
            //Start frame capture
            Integer triggerId;
            try {
                // Activate loop frame capture to get trigger id
                triggerId = camera.triggerLoopingCapture(previewFrameConfig);
            } catch (IllegalArgumentException e) {

            } catch (IllegalStateException e) {

            }
        }

        /**
         * Camera creation failed callback
         *
         * @param cameraId
         * @param errorCode
         */
        @Override
        public void onCreateFailed(String cameraId, int errorCode) {

        }

        /**
         * Release camera object callback
         *
         * @param camera
         */
        @Override
        public void onReleased(Camera camera) {
            // Release camera equipment
            camera.release();
        }
    }

}
