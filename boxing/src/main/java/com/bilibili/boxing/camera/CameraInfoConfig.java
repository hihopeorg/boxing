/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.camera;


import com.bilibili.boxing.camera.util.CameraRatioUtil;
import ohos.app.Context;
import ohos.media.image.common.Size;

import java.util.List;

/**
 * Camera Initialization, Configuration Class
 */
public class CameraInfoConfig {

    /**
     * Camera Proportion
     * Default ratio 4:3
     */
    public static final Integer cameraRatio = CameraRatioUtil.FULL_SCREEN;

    /**
     * Camera preview photo area length and width value, according to camera ratio calculation
     */
    public static Size cameraSize;

    /**
     * Default rear camera
     */
    public static final Integer cameraType = 0;

    /**
     * Camera-supported resolution list
     */
    public static List<Size> pictureSizes;

    /**
     * The best resolution value is obtained from the list of resolution supported by the camera based on the camera area length and width value
     */
    public static Size pictureOptimumSize;

    /**
     * Initialize camera information
     *
     * @param context
     */
    public static void initCameraConfig(Context context) {
        //Initialize camera length and width
        cameraSize = CameraRatioUtil.getCameraWidth(context, cameraRatio);
    }

}
