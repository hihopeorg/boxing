/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing;

import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.presenter.PickerContract;
import com.bilibili.boxing.presenter.PickerPresenter;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;

/**
 * A abstract class to connect {@link PickerContract.View} and {@link PickerContract.Presenter}.
 * one job has to be done. override {@link #onCreateBoxingView(ArrayList)} to create a subclass for {@link AbsBoxingViewFragment}.
 */
public abstract class AbsBoxingActivity extends FractionAbility implements Boxing.OnBoxingFinishListener {

    private AbsBoxingViewFragment mAbsBoxingViewFragment;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        mAbsBoxingViewFragment = onCreateBoxingView(getSelectedMedias(getIntent()));
        BoxingConfig pickerConfig = BoxingManager.getInstance().getBoxingConfig();
        mAbsBoxingViewFragment.setPresenter(new PickerPresenter(mAbsBoxingViewFragment));
        mAbsBoxingViewFragment.setPickerConfig(pickerConfig);
        Boxing.get().setupFragment(mAbsBoxingViewFragment, this);
    }

    private ArrayList<BaseMedia> getSelectedMedias(Intent intent) {
        return intent.getSequenceableArrayListParam(Boxing.EXTRA_SELECTED_MEDIA);
    }

    public BoxingConfig getBoxingConfig() {
        return BoxingManager.getInstance().getBoxingConfig();
    }

    /**
     * create a {@link PickerContract.View} attaching to
     * {@link PickerContract.Presenter},call in {@link #onStart(Intent)}
     */
    public abstract AbsBoxingViewFragment onCreateBoxingView(ArrayList<BaseMedia> medias);
}
