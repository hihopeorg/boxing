 /*
  *  Copyright (C) 2017 Bilibili
  *
  *  Licensed under the Apache License, Version 2.0 (the "License");
  *  you may not use this file except in compliance with the License.
  *  You may obtain a copy of the License at
  *
  *          http://www.apache.org/licenses/LICENSE-2.0
  *
  *  Unless required by applicable law or agreed to in writing, software
  *  distributed under the License is distributed on an "AS IS" BASIS,
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  */

 package com.bilibili.boxing.model.entity;


 import ohos.utils.Parcel;
 import ohos.utils.Sequenceable;

 /**
  * The base entity for media.
  */
 public abstract class BaseMedia implements Sequenceable {
     protected enum TYPE {
         IMAGE, VIDEO
     }

     protected String mPath;
     protected String mId;
     protected String mSize;

     public BaseMedia() {
     }

     public BaseMedia(String id, String path) {
         mId = id;
         mPath = path;
     }

     public abstract TYPE getType();

     public String getId() {
         return mId;
     }

     public long getSize() {
         try {
             long result = Long.parseLong(mSize);
             return result > 0 ? result : 0;
         } catch (NumberFormatException size) {
             return 0;
         }
     }

     public void setId(String id) {
         mId = id;
     }

     public void setSize(String size) {
         mSize = size;
     }

     public String getPath() {
         return mPath;
     }

     public void setPath(String path) {
         mPath = path;
     }

     @Override
     public boolean marshalling(Parcel parcel) {
         return parcel.writeString(this.mPath)
                 && parcel.writeString(this.mId)
                 && parcel.writeString(this.mSize);
     }

     @Override
     public boolean unmarshalling(Parcel parcel) {
         this.mPath = parcel.readString();
         this.mId = parcel.readString();
         this.mSize = parcel.readString();
         return true;
     }

     public static final Sequenceable.Producer
             PRODUCER = new Sequenceable.Producer
             () {
         public BaseMedia createFromParcel(Parcel in) {
             // Initialize an instance first, then do customized unmarshlling.
             BaseMedia instance = new BaseMedia() {
                 @Override
                 public TYPE getType() {
                     return TYPE.IMAGE;
                 }
             };
             instance.unmarshalling(in);
             return instance;
         }
     };

 }
