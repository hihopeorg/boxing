/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.model.task.impl;

import com.bilibili.boxing.model.callback.IMediaTaskCallback;
import com.bilibili.boxing.model.entity.impl.VideoMedia;
import com.bilibili.boxing.model.task.IMediaTask;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.photokit.metadata.AVStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * A Task to load {@link VideoMedia} in database.
 */
public class VideoTask implements IMediaTask<VideoMedia> {

    private final static String[] MEDIA_COL = new String[]{
            AVStorage.Video.Media.DATA,
            AVStorage.Video.Media.ID,
            AVStorage.Video.Media.TITLE,
            AVStorage.Video.Media.MIME_TYPE,
            AVStorage.Video.Media.SIZE,
            AVStorage.Video.Media.DATE_ADDED,
            AVStorage.Video.Media.DURATION
    };

    @Override
    public void load(final DataAbilityHelper cr, final int page, String id, final IMediaTaskCallback<VideoMedia> callback) {
        loadVideos(cr, page, callback);
    }

    private void loadVideos(DataAbilityHelper cr, int page, final IMediaTaskCallback<VideoMedia> callback) {
        final List<VideoMedia> videoMedias = new ArrayList<>();

        // Constructing Query Conditions
        DataAbilityPredicates predicates = new DataAbilityPredicates();
        predicates.setOrder(AVStorage.Images.Media.DATE_MODIFIED + " desc" + " LIMIT " + page * IMediaTask.PAGE_LIMIT + " , " + IMediaTask.PAGE_LIMIT);
        ResultSet cursor = null;
        try {
            cursor = cr.query(AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI, MEDIA_COL, null);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        try {
            int count = 0;
            if (cursor != null && cursor.goToFirstRow()) {
                count = cursor.getRowCount();
                do {
                    String data = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.DATA));
                    String id = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.ID));
                    String title = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.TITLE));
                    String type = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.MIME_TYPE));
                    String size = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.SIZE));
                    String date = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.DATE_ADDED));
                    String duration = cursor.getString(cursor.getColumnIndexForName(AVStorage.Video.Media.DURATION));
                    VideoMedia video = new VideoMedia.Builder(id, data).setTitle(title).setDuration(duration)
                            .setSize(size).setDataTaken(date).setMimeType(type).build();
                    videoMedias.add(video);

                } while (!cursor.isAtLastRow() && cursor.goToNextRow());
                postMedias(callback, videoMedias, count);
            } else {
                postMedias(callback, videoMedias, 0);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void postMedias(final IMediaTaskCallback<VideoMedia> callback,
                            final List<VideoMedia> videoMedias, final int count) {
        callback.postMedia(videoMedias, count);
    }

}
