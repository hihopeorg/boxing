/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.model.task.impl;


import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.callback.IAlbumTaskCallback;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.AlbumEntity;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.media.photokit.metadata.AVStorage.Images.Media;
import ohos.utils.LightweightMap;

import java.util.*;

/**
 * A task to load albums.
 */
public class AlbumTask {
    private static final String UNKNOWN_ALBUM_NAME = "unknow";
    private static final String SELECTION_IMAGE_MIME_TYPE = Media.MIME_TYPE + "=? or " + Media.MIME_TYPE + "=? or " + Media.MIME_TYPE + "=? or " + Media.MIME_TYPE + "=?";
    private static final String SELECTION_IMAGE_MIME_TYPE_WITHOUT_GIF = Media.MIME_TYPE + "=? or " + Media.MIME_TYPE + "=? or " + Media.MIME_TYPE + "=?";
    private static final String SELECTION_ID = Media.ID + "=? and (" + SELECTION_IMAGE_MIME_TYPE + " )";
    private static final String SELECTION_ID_WITHOUT_GIF = Media.ID + "=? and (" + SELECTION_IMAGE_MIME_TYPE_WITHOUT_GIF + " )";
    private static final String[] SELECTION_ARGS_IMAGE_MIME_TYPE = {"image/jpeg", "image/png", "image/jpg", "image/gif"};
    private static final String[] SELECTION_ARGS_IMAGE_MIME_TYPE_WITHOUT_GIF = {"image/jpeg", "image/png", "image/jpg"};
    private int mUnknownAlbumNumber = 1;
    private Map<String, AlbumEntity> mBucketMap;
    private AlbumEntity mDefaultAlbum;
    private BoxingConfig mPickerConfig;

    public AlbumTask() {
        this.mBucketMap = new LightweightMap<>();
        this.mDefaultAlbum = AlbumEntity.createDefaultAlbum();
        this.mPickerConfig = BoxingManager.getInstance().getBoxingConfig();
    }

    public void start(final DataAbilityHelper cr, final IAlbumTaskCallback callback) {
        buildAlbumInfo(cr);
        getAlbumList(callback);
    }

    private List<BaseMedia> mAllImageList = new ArrayList<>();

    private void buildAlbumInfo(DataAbilityHelper cr) {
        String[] distinctBucketColumns = new String[]{AVStorage.Images.Media.ID, AVStorage.Images.Media.DISPLAY_NAME};
        ResultSet bucketCursor = null;
        try {
            // Constructing Query Conditions
            DataAbilityPredicates predicates = new DataAbilityPredicates("");
            predicates.setOrder(Media.DATE_MODIFIED + " desc");
            bucketCursor = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID, AVStorage.Images.Media.DISPLAY_NAME, AVStorage.Images.Media.DATA}, null);
            if (bucketCursor != null && bucketCursor.goToFirstRow()) {
                do {
                    String buckId = bucketCursor.getString(bucketCursor.getColumnIndexForName(AVStorage.Images.Media.ID));
                    String name = bucketCursor.getString(bucketCursor.getColumnIndexForName(AVStorage.Images.Media.DISPLAY_NAME));
                    String path = bucketCursor.getString(bucketCursor.getColumnIndexForName(AVStorage.Images.Media.DATA));
                    mAllImageList.add(new ImageMedia(buckId, path));
                    String[] str = path.split("/");
                    String albumName = str[5];
                    AlbumEntity album = buildAlbumInfo(albumName, buckId);
                    if (!buckId.isEmpty()) {
                        buildAlbumCover(cr, albumName, buckId, album);
                    }
                } while (bucketCursor.goToNextRow());
            }
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } finally {
            if (bucketCursor != null) {
                bucketCursor.close();
            }
        }
    }

    /**
     * get the cover and count
     *
     * @param buckId album id
     */
    private void buildAlbumCover(DataAbilityHelper cr, String albumName, String buckId, AlbumEntity album) {
        String[] photoColumn = new String[]{Media.ID, Media.DATA};
        boolean isNeedGif = mPickerConfig != null && mPickerConfig.isNeedGif();
        String selectionId = isNeedGif ? SELECTION_ID : SELECTION_ID_WITHOUT_GIF;
        String[] args = isNeedGif ? SELECTION_ARGS_IMAGE_MIME_TYPE : SELECTION_ARGS_IMAGE_MIME_TYPE_WITHOUT_GIF;
        String[] selectionArgs = new String[args.length + 1];
        selectionArgs[0] = buckId;
        for (int i = 1; i < selectionArgs.length; i++) {
            selectionArgs[i] = args[i - 1];
        }
        List<String> selectionArgsList = Arrays.asList(selectionArgs);

        // Constructing Query Conditions
        DataAbilityPredicates predicates = new DataAbilityPredicates(selectionId);
        predicates.setWhereArgs(selectionArgsList);
        predicates.setOrder(Media.DATE_MODIFIED + " desc");

        ResultSet coverCursor = null;
        try {
            coverCursor = cr.query(Media.EXTERNAL_DATA_ABILITY_URI, photoColumn, null);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
        try {
            if (coverCursor != null && coverCursor.goToFirstRow()) {
                String picPath = coverCursor.getString(coverCursor.getColumnIndexForName(Media.DATA));
                String id = coverCursor.getString(coverCursor.getColumnIndexForName(Media.ID));
                album.mImageList.add(new ImageMedia(buckId, picPath));
                if (album.mImageList.size() > 0) { //从这里修改
                    mBucketMap.put(albumName, album);
                }
            }
        } finally {
            if (coverCursor != null) {
                coverCursor.close();
            }
        }
    }

    private void getAlbumList(final IAlbumTaskCallback callback) {
        mDefaultAlbum.mCount = 0;
        List<AlbumEntity> tmpList = new ArrayList<>();
        if (mBucketMap == null) {
            postAlbums(callback, tmpList);
            return;
        }

        for (Map.Entry<String, AlbumEntity> entry : mBucketMap.entrySet()) {
            AlbumEntity albumEntity = entry.getValue();
            List<BaseMedia> baseMediaList = new ArrayList<>();
            for (int i = 0; i < mAllImageList.size(); i++) {
                if (mAllImageList.get(i).getPath().contains(entry.getValue().mBucketName)) {
                    baseMediaList.add(mAllImageList.get(i));
                }
            }
            mDefaultAlbum.mCount = mAllImageList.size();
            albumEntity.mCount = baseMediaList.size();
            albumEntity.mImageList = baseMediaList;
            tmpList.add(albumEntity);
        }
        Collections.reverse(tmpList);
        if (tmpList.size() > 0 && tmpList.get(0) != null) {
            mDefaultAlbum.mImageList = mAllImageList;
            tmpList.add(0, mDefaultAlbum);
        }
        postAlbums(callback, tmpList);
        clear();
    }

    /**
     * Album Classification
     *
     * @param callback
     * @param result
     */
    private void postAlbums(final IAlbumTaskCallback callback, final List<AlbumEntity> result) {
        callback.postAlbumList(result);
    }

    private AlbumEntity buildAlbumInfo(String bucketName, String bucketId) {
        AlbumEntity album = null;
        if (!bucketId.isEmpty()) {
            album = mBucketMap.get(bucketId);
        }
        if (album == null) {
            album = new AlbumEntity();
            if (!bucketId.isEmpty()) {
                album.mBucketId = bucketId;
            } else {
                album.mBucketId = String.valueOf(mUnknownAlbumNumber);
                mUnknownAlbumNumber++;
            }
            if (!bucketName.isEmpty()) {
                album.mBucketName = bucketName;
            } else {
                album.mBucketName = UNKNOWN_ALBUM_NAME;
                mUnknownAlbumNumber++;
            }
            if (album.mImageList.size() > 0) {
                mBucketMap.put(bucketId, album);
            }
        }
        return album;
    }

    private void clear() {
        if (mBucketMap != null) {
            mBucketMap.clear();
        }
    }

}
