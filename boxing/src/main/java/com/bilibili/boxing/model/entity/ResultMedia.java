/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 package com.bilibili.boxing.model.entity;


 import ohos.utils.Parcel;
 import ohos.utils.Sequenceable;

 public class ResultMedia implements Sequenceable {

     protected String id;

     public ResultMedia() {

     }

     public void setId(String id) {
         this.id = id;
     }

     public String getId() {
         return id;
     }

     @Override
     public boolean marshalling(Parcel parcel) {
         return parcel.writeString(this.id);
     }

     @Override
     public boolean unmarshalling(Parcel parcel) {
         this.id = parcel.readString();
         return true;
     }

     public static final Producer
             PRODUCER = new Producer
             () {
         public ResultMedia createFromParcel(Parcel in) {
             // Initialize an instance first, then do customized unmarshlling.
             ResultMedia instance = new ResultMedia();
             instance.unmarshalling(in);
             return instance;
         }
     };

 }
