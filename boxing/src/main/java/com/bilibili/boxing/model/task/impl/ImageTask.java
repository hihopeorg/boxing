/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.model.task.impl;


import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.callback.IMediaTaskCallback;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing.model.task.IMediaTask;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.LightweightMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * A Task to load photos.
 */
public class ImageTask implements IMediaTask<ImageMedia> {
    private static final String CONJUNCTION_SQL = "=? or";
    private static final String SELECTION_IMAGE_MIME_TYPE = AVStorage.Images.Media.MIME_TYPE + CONJUNCTION_SQL + " " + AVStorage.Images.Media.MIME_TYPE + CONJUNCTION_SQL + " " + AVStorage.Images.Media.MIME_TYPE + CONJUNCTION_SQL + " " + AVStorage.Images.Media.MIME_TYPE + "=?";
    private static final String SELECTION_IMAGE_MIME_TYPE_WITHOUT_GIF = AVStorage.Images.Media.MIME_TYPE + CONJUNCTION_SQL + " " + AVStorage.Images.Media.MIME_TYPE + CONJUNCTION_SQL + " " + AVStorage.Images.Media.MIME_TYPE + "=?";
    private static final String SELECTION_ID = AVStorage.Images.Media.ID + "=? and (" + SELECTION_IMAGE_MIME_TYPE + " )";
    private static final String SELECTION_ID_WITHOUT_GIF = AVStorage.Images.Media.ID + "=? and (" + SELECTION_IMAGE_MIME_TYPE_WITHOUT_GIF + " )";

    private static final String IMAGE_JPEG = "image/jpeg";
    private static final String IMAGE_PNG = "image/png";
    private static final String IMAGE_JPG = "image/jpg";
    private static final String IMAGE_GIF = "image/gif";
    private static final String[] SELECTION_ARGS_IMAGE_MIME_TYPE = {IMAGE_JPEG, IMAGE_PNG, IMAGE_JPG, IMAGE_GIF};
    private static final String[] SELECTION_ARGS_IMAGE_MIME_TYPE_WITHOUT_GIF = {IMAGE_JPEG, IMAGE_PNG, IMAGE_JPG};

    private static final String DESC = " desc";

    private BoxingConfig mPickerConfig;
    private Map<String, String> mThumbnailMap;

    public ImageTask() {
        this.mThumbnailMap = new LightweightMap<>();
        this.mPickerConfig = BoxingManager.getInstance().getBoxingConfig();
    }

    @Override
    public void load(final DataAbilityHelper cr, final int page, final String id, final IMediaTaskCallback<ImageMedia> callback) {
        buildThumbnail(cr);
        buildAlbumList(cr, id, page, callback);
    }

    private void buildThumbnail(DataAbilityHelper cr) {
        String[] projection = {AVStorage.Images.Media.ID, AVStorage.Images.Media.DATA};
        queryThumbnails(cr, projection);
    }

    private void queryThumbnails(DataAbilityHelper cr, String[] projection) {
        ResultSet cur = null;
        try {
            // Constructing Query Conditions
            try {
                cur = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID, AVStorage.Images.Media.DATA}, null);
            } catch (DataAbilityRemoteException e) {
                e.printStackTrace();
            }
            if (cur != null && cur.goToFirstRow()) {
                do {
                    String imageId = cur.getString(cur.getColumnIndexForName(AVStorage.Images.Media.ID));
                    String imagePath = cur.getString(cur.getColumnIndexForName(AVStorage.Images.Media.DATA));
                    mThumbnailMap.put(imageId, imagePath);
                } while (cur.goToNextRow() && !cur.isAtLastRow());
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private List<ImageMedia> buildAlbumList(DataAbilityHelper cr, String bucketId, int page, final IMediaTaskCallback<ImageMedia> callback) {
        List<ImageMedia> result = new ArrayList<>();
        String columns[] = getColumns();
        ResultSet cursor = null;
        try {
            boolean isDefaultAlbum = bucketId.isEmpty();
            boolean isNeedPaging = mPickerConfig == null || mPickerConfig.isNeedPaging();
            boolean isNeedGif = mPickerConfig != null && mPickerConfig.isNeedGif();
            int totalCount = getTotalCount(cr, bucketId, columns, isDefaultAlbum, isNeedGif);

            String imageMimeType = isNeedGif ? SELECTION_IMAGE_MIME_TYPE : SELECTION_IMAGE_MIME_TYPE_WITHOUT_GIF;
            String[] args = isNeedGif ? SELECTION_ARGS_IMAGE_MIME_TYPE : SELECTION_ARGS_IMAGE_MIME_TYPE_WITHOUT_GIF;
            List<String> argsList = Arrays.asList(args);

            String order = isNeedPaging ? AVStorage.Images.Media.DATE_MODIFIED + DESC + " LIMIT "
                    + page * IMediaTask.PAGE_LIMIT + " , " + IMediaTask.PAGE_LIMIT : AVStorage.Images.Media.DATE_MODIFIED + DESC;
            String selectionId = isNeedGif ? SELECTION_ID : SELECTION_ID_WITHOUT_GIF;
            cursor = query(cr, bucketId, columns, isDefaultAlbum, isNeedGif, imageMimeType, argsList, order, selectionId);
            addItem(totalCount, result, cursor, callback);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    private void addItem(final int allCount, final List<ImageMedia> result, ResultSet cursor, final IMediaTaskCallback<ImageMedia> callback) {
        if (cursor != null && cursor.goToFirstRow()) {
            do {
                String picPath = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.DATA));
                if (callback.needFilter(picPath)) {

                } else {
                    String id = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.ID));
                    String size = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.SIZE));
                    String mimeType = cursor.getString(cursor.getColumnIndexForName(AVStorage.Images.Media.MIME_TYPE));
                    int width = 1080;
                    int height = 2400;
                    ImageMedia imageItem = new ImageMedia.Builder(id, picPath).setThumbnailPath(mThumbnailMap.get(id))
                            .setSize(size).setMimeType(mimeType).setHeight(height).setWidth(width).build();
                    if (!result.contains(imageItem)) {
                        result.add(imageItem);
                    }
                }
            } while (!cursor.isAtLastRow() && cursor.goToNextRow());
            postMedias(result, allCount, callback);
        } else {
            postMedias(result, 0, callback);
        }
        clear();
    }

    private void postMedias(final List<ImageMedia> result, final int count, final IMediaTaskCallback<ImageMedia> callback) {
        callback.postMedia(result, count);
    }

    private ResultSet query(DataAbilityHelper cr, String bucketId, String[] columns, boolean isDefaultAlbum, boolean isNeedGif, String imageMimeType, List<String> args, String order, String selectionId) {
        ResultSet resultCursor = null;
        if (isDefaultAlbum) {
            // Constructing Query Conditions
            DataAbilityPredicates predicates = new DataAbilityPredicates(imageMimeType);
            predicates.setWhereArgs(args);
            predicates.setOrder(order);
            try {
                resultCursor = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, columns, null);
            } catch (DataAbilityRemoteException e) {
                e.printStackTrace();
            }
        } else {
            if (isNeedGif) {
                List<String> list = new ArrayList<>();
                list.add(args.get(0));
                list.add(args.get(1));
                list.add(args.get(2));
                list.add(args.get(3));
                // Constructing Query Conditions
                DataAbilityPredicates predicates = new DataAbilityPredicates(selectionId);
                predicates.setWhereArgs(list);
                predicates.setOrder(order);
                try {
                    resultCursor = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, columns, null);
                } catch (DataAbilityRemoteException e) {
                    e.printStackTrace();
                }
            } else {
                List<String> list = new ArrayList<>();
                list.add(args.get(0));
                list.add(args.get(1));
                list.add(args.get(2));
                // Constructing Query Conditions
                DataAbilityPredicates predicates = new DataAbilityPredicates(selectionId);
                predicates.setWhereArgs(list);
                predicates.setOrder(order);
                try {
                    resultCursor = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, columns, null);
                } catch (DataAbilityRemoteException e) {
                    e.printStackTrace();
                }
            }
        }
        return resultCursor;
    }

    private String[] getColumns() {
        String[] columns;
        columns = new String[]{AVStorage.Images.Media.ID, AVStorage.Images.Media.DATA, AVStorage.Images.Media.SIZE, AVStorage.Images.Media.MIME_TYPE};
        return columns;
    }

    private int getTotalCount(DataAbilityHelper cr, String bucketId, String[] columns, boolean isDefaultAlbum, boolean isNeedGif) {
        ResultSet allCursor = null;
        int result = 0;
        try {
            if (isDefaultAlbum) {
                // Constructing Query Conditions
                DataAbilityPredicates predicates = new DataAbilityPredicates(SELECTION_IMAGE_MIME_TYPE);
                predicates.setWhereArgs(Arrays.asList(SELECTION_ARGS_IMAGE_MIME_TYPE));
                predicates.setOrder(AVStorage.Images.Media.DATE_MODIFIED + DESC);
                try {
                    allCursor = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, columns, null);
                } catch (DataAbilityRemoteException e) {
                    e.printStackTrace();
                }
            } else {
                if (isNeedGif) {
                    // Constructing Query Conditions
                    DataAbilityPredicates predicates = new DataAbilityPredicates(SELECTION_ID);
                    predicates.setWhereArgs(Arrays.asList(new String[]{bucketId, IMAGE_JPEG, IMAGE_PNG, IMAGE_JPG, IMAGE_GIF}));
                    predicates.setOrder(AVStorage.Images.Media.DATE_MODIFIED + DESC);
                    try {
                        allCursor = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, columns, null);
                    } catch (DataAbilityRemoteException e) {
                        e.printStackTrace();
                    }
                } else {
                    // Constructing Query Conditions
                    DataAbilityPredicates predicates = new DataAbilityPredicates(SELECTION_ID_WITHOUT_GIF);
                    predicates.setWhereArgs(Arrays.asList(new String[]{bucketId, IMAGE_JPEG, IMAGE_PNG, IMAGE_JPG}));
                    predicates.setOrder(AVStorage.Images.Media.DATE_MODIFIED + DESC);
                    try {
                        allCursor = cr.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, columns, null);
                    } catch (DataAbilityRemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (allCursor != null) {
                result = allCursor.getRowCount();
            }
        } finally {
            if (allCursor != null) {
                allCursor.close();
            }
        }
        return result;
    }

    private void clear() {
        if (mThumbnailMap != null) {
            mThumbnailMap.clear();
        }
    }

}
