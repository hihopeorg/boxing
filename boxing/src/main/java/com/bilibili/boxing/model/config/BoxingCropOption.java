/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.model.config;


import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

/**
 * The cropping config, a cropped photo uri is needed at least.
 */
public class BoxingCropOption implements Sequenceable {
    private Uri mDestination;
    private float mAspectRatioX;
    private float mAspectRatioY;
    private int mMaxWidth;
    private int mMaxHeight;
    private boolean mCropMode;

    public BoxingCropOption() {

    }

    public BoxingCropOption(Uri destination) {
        this.mDestination = destination;
    }

    public static BoxingCropOption with(Uri destination) {
        return new BoxingCropOption(destination);
    }

    public BoxingCropOption aspectRatio(float x, float y) {
        this.mAspectRatioX = x;
        this.mAspectRatioY = y;
        return this;
    }

    public BoxingCropOption useSourceImageAspectRatio() {
        this.mAspectRatioX = 0;
        this.mAspectRatioY = 0;
        return this;
    }

    public BoxingCropOption withMaxResultSize(int width, int height) {
        this.mMaxWidth = width;
        this.mMaxHeight = height;
        return this;
    }

    public boolean getCropMode() {
        return mCropMode;
    }

    public void isCircleMode(boolean mCropMode) {
        this.mCropMode = mCropMode;
    }

    public float getAspectRatioX() {
        return mAspectRatioX;
    }

    public float getAspectRatioY() {
        return mAspectRatioY;
    }

    public int getMaxHeight() {
        return mMaxHeight;
    }

    public int getMaxWidth() {
        return mMaxWidth;
    }

    public Uri getDestination() {
        return mDestination;
    }


    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeSequenceable(this.mDestination);
        return parcel.writeFloat(this.mAspectRatioX)
                && parcel.writeFloat(this.mAspectRatioY)
                && parcel.writeInt(this.mMaxWidth)
                && parcel.writeInt(this.mMaxHeight);
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        this.mAspectRatioX = parcel.readFloat();
        this.mAspectRatioY = parcel.readFloat();
        this.mMaxWidth = parcel.readInt();
        this.mMaxHeight = parcel.readInt();
        return parcel.readSequenceable(this.mDestination);
    }

    public static final Sequenceable.Producer
            PRODUCER = new Sequenceable.Producer
            () {
        public BoxingCropOption createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            BoxingCropOption instance = new BoxingCropOption();
            instance.unmarshalling(in);
            return instance;
        }
    };

}