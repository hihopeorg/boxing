/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.model.entity;


import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.util.ArrayList;
import java.util.List;


/**
 * An entity for album.
 */
public class AlbumEntity implements Sequenceable {
    public static final String DEFAULT_NAME = "";

    public int mCount;
    public boolean mIsSelected;

    public String mBucketId;
    public String mBucketName = "";
    public List<BaseMedia> mImageList;

    public AlbumEntity() {
        mCount = 0;
        mImageList = new ArrayList<>();
        mIsSelected = false;
    }

    public static AlbumEntity createDefaultAlbum() {
        AlbumEntity result = new AlbumEntity();
        result.mBucketId = DEFAULT_NAME;
        result.mIsSelected = true;
        return result;
    }

    public boolean hasImages() {
        return mImageList != null && mImageList.size() > 0;
    }

    @Override
    public String toString() {
        return "AlbumEntity{" +
                "mCount=" + mCount +
                ", mBucketName='" + mBucketName + '\'' +
                ", mImageList=" + mImageList +
                '}';
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeList(this.mImageList);
        return parcel.writeString(this.mBucketId)
                && parcel.writeInt(this.mCount)
                && parcel.writeString(this.mBucketName)
                && parcel.writeByte(this.mIsSelected ? (byte) 1 : (byte) 0);
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        this.mBucketId = parcel.readString();
        this.mCount = parcel.readInt();
        this.mBucketName = parcel.readString();
        this.mImageList = new ArrayList<>();
        this.mImageList = (List<BaseMedia>) parcel.readList();
        this.mIsSelected = parcel.readByte() != 0;
        return true;
    }

    public static final Sequenceable.Producer
            PRODUCER = new Sequenceable.Producer
            () {
        public AlbumEntity createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            AlbumEntity instance = new AlbumEntity();
            instance.unmarshalling(in);
            return instance;
        }
    };

}
