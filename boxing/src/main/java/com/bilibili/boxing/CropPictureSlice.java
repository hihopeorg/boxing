/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing;

import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.UCropView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.data.rdb.ValuesBucket;
import ohos.global.icu.text.DecimalFormat;
import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class CropPictureSlice extends AbilitySlice {
    //General layout
    private DirectionalLayout directionalLayout = new DirectionalLayout(this);
    private DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
    //Zoom and rotate control layout
    private DirectionalLayout rotationController = new DirectionalLayout(this);
    private DirectionalLayout.LayoutConfig rotationConfig = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
    private DirectionalLayout scaleController = new DirectionalLayout(this);
    private DirectionalLayout.LayoutConfig scaleConfig = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
    //uCrop
    private UCropView uCropView;
    private GestureCropImageView mGestureCropImageView;
    //Scaling and rotation parameters
    private float mDegree;
    private float mFactor;
    private Text text;
    private Text text2;
    private DecimalFormat df = new DecimalFormat("#.00");

    //URI of pictures to be cut
    public Uri uri_i;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        //URI of network diagram
//        uri_i = Uri.parse("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fa0.att.hudong.com%2F30%2F29%2F01300000201438121627296084016.jpg&refer=http%3A%2F%2Fa0.att.hudong.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1617882104&t=a1fdb27b4652aee1f5f48fc8d024a1bb");
        uri_i = getAbility().getIntent().getUri();
        //Local URI
        String filename = "test.jpg";
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(100, 100);
        PixelMap pixelmap = PixelMap.create(options);
        Uri uri_o = saveImage(filename, pixelmap);
        //UcropView
        uCropView = new UCropView(this);
        try {
            uCropView.getCropImageView().setImageUri(uri_i, uri_o);
            uCropView.getOverlayView().setShowCropFrame(true);
            uCropView.getOverlayView().setShowCropGrid(true);
            uCropView.getOverlayView().setDimmedColor(Color.TRANSPARENT.getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Global context
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(Color.LTGRAY.getValue()));
        //Button background
        ShapeElement buttonBackground = new ShapeElement();
        buttonBackground.setRgbColor(new RgbColor(Color.CYAN.getValue()));
        buttonBackground.setCornerRadius(50f);
        //Button cutting
        Button button = new Button(this);
        button.setText("裁剪");
        button.setTextSize(100);
        button.setMarginTop(50);
        button.setPadding(50, 50, 50, 50);
        button.setBackground(buttonBackground);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                cropAndSaveImage();
                component.setVisibility(Component.INVISIBLE);
            }
        });

        //90 Button rotation
        Button button_plus_90 = new Button(this);
        button_plus_90.setText("+90°");
        button_plus_90.setTextSize(80);
        button_plus_90.setBackground(buttonBackground);
        button_plus_90.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                float degrees = 90f;
                //Calculation of rotation centre
                float center_X = uCropView.getOverlayView().getCropViewRect().getCenter().getPointX();
                float center_Y = uCropView.getOverlayView().getCropViewRect().getCenter().getPointY();
                //Rotation
                uCropView.getCropImageView().postRotate(degrees, center_X, center_Y);
                //Adaptation
                uCropView.getCropImageView().setImageToWrapCropBounds(false);
                //Display rotation angle
                mDegree = uCropView.getCropImageView().getCurrentAngle();
                text.setText("当前旋转角度: " + df.format(mDegree) + " °");
            }
        });
        //Rotary -90 Button
        Button button_minus_90 = new Button(this);
        button_minus_90.setText("-90°");
        button_minus_90.setTextSize(80);
        button_minus_90.setBackground(buttonBackground);
        button_minus_90.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                float degrees = -90f;
                //Calculation of rotation centre
                float center_X = uCropView.getOverlayView().getCropViewRect().getCenter().getPointX();
                float center_Y = uCropView.getOverlayView().getCropViewRect().getCenter().getPointY();
                //Rotation
                uCropView.getCropImageView().postRotate(degrees, center_X, center_Y);
                //Adaptation
                uCropView.getCropImageView().setImageToWrapCropBounds(false);
                //Display rotation angle
                mDegree = uCropView.getCropImageView().getCurrentAngle();
                text.setText("当前旋转角度: " + df.format(mDegree) + " °");
            }
        });

        //Slider background
        ShapeElement sliderBackground = new ShapeElement();
        sliderBackground.setRgbColor(new RgbColor(Color.BLUE.getValue()));
        //Slider of rotation
        Slider slider_rotation = new Slider(this);
        slider_rotation.setWidth(800);
        slider_rotation.setProgressValue(50);
        slider_rotation.setProgressColor(Color.BLUE);
        slider_rotation.setProgressBackgroundElement(sliderBackground);
        slider_rotation.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                float degrees = i - 50f;
                //Calculation of rotation centre
                float center_X = uCropView.getOverlayView().getCropViewRect().getCenter().getPointX();
                float center_Y = uCropView.getOverlayView().getCropViewRect().getCenter().getPointY();
                //Rotation
                uCropView.getCropImageView().postRotate(0.05f * degrees, center_X, center_Y);
                //Display rotation angle
                mDegree = uCropView.getCropImageView().getCurrentAngle();
                text.setText("当前旋转角度: " + df.format(mDegree) + " °");
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                uCropView.getCropImageView().setImageToWrapCropBounds(false);
                slider.setProgressValue(50);
                //Adaptive scaling multiples
                mFactor = uCropView.getCropImageView().getCurrentScale();
                text2.setText("当前缩放倍数: x" + df.format(mFactor));
            }
        });
        rotationController.setAlignment(LayoutAlignment.HORIZONTAL_CENTER);
        rotationController.setLayoutConfig(rotationConfig);
        rotationController.setOrientation(Component.HORIZONTAL);
        rotationController.addComponent(button_minus_90);
        rotationController.addComponent(slider_rotation);
        rotationController.addComponent(button_plus_90);

        //Scaling x2Button
        Button button_plus_2 = new Button(this);
        button_plus_2.setText("x2");
        button_plus_2.setTextSize(80);
        button_plus_2.setBackground(buttonBackground);
        button_plus_2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                float factor = 2f;
                //Calculation of rotation centre
                float center_X = uCropView.getOverlayView().getCropViewRect().getCenter().getPointX();
                float center_Y = uCropView.getOverlayView().getCropViewRect().getCenter().getPointY();
                //Rotation
                uCropView.getCropImageView().postScale(factor, center_X, center_Y);
                //适配
                uCropView.getCropImageView().setImageToWrapCropBounds(false);
                //Display scaling multiples
                mFactor = uCropView.getCropImageView().getCurrentScale();
                text2.setText("当前缩放倍数: x" + df.format(mFactor));
            }
        });

        //Zoom /2 Button
        Button button_minus_2 = new Button(this);
        button_minus_2.setText("/2");
        button_minus_2.setTextSize(80);
        button_minus_2.setBackground(buttonBackground);
        button_minus_2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                float factor = 0.5f;
                //Calculation of rotation centre
                float center_X = uCropView.getOverlayView().getCropViewRect().getCenter().getPointX();
                float center_Y = uCropView.getOverlayView().getCropViewRect().getCenter().getPointY();
                //Rotation
                uCropView.getCropImageView().postScale(factor, center_X, center_Y);
                //Adaptation
                uCropView.getCropImageView().setImageToWrapCropBounds(false);
                //Display scaling multiples
                mFactor = uCropView.getCropImageView().getCurrentScale();
                text2.setText("当前缩放倍数: x" + df.format(mFactor));
            }
        });

        //Scaling Slider
        Slider slider_Scale = new Slider(this);
        slider_Scale.setWidth(800);
        slider_Scale.setProgressValue(50);
        slider_Scale.setProgressColor(Color.BLUE);
        slider_Scale.setProgressBackgroundElement(sliderBackground);
        slider_Scale.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                float factor = i - 50f;
                //Calculation of rotation centre
                float center_X = uCropView.getOverlayView().getCropViewRect().getCenter().getPointX();
                float center_Y = uCropView.getOverlayView().getCropViewRect().getCenter().getPointY();
                //Rotation
                uCropView.getCropImageView().postScale((float) Math.exp(0.001f * factor), center_X, center_Y);
                //Display scaling multiples
                mFactor = uCropView.getCropImageView().getCurrentScale();
                text2.setText("当前缩放倍数: x" + df.format(mFactor));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                uCropView.getCropImageView().setImageToWrapCropBounds(false);
                slider.setProgressValue(50);
                //Adaptive scaling multiples
                mFactor = uCropView.getCropImageView().getCurrentScale();
                text2.setText("当前缩放倍数: x" + df.format(mFactor));

            }
        });
        scaleController.setAlignment(LayoutAlignment.HORIZONTAL_CENTER);
        scaleController.setLayoutConfig(scaleConfig);
        scaleController.setOrientation(Component.HORIZONTAL);
        scaleController.addComponent(button_minus_2);
        scaleController.addComponent(slider_Scale);
        scaleController.addComponent(button_plus_2);

        //Text of rotation
        text = new Text(this);
        text.setText("当前旋转角度: " + df.format(mDegree) + " °");
        text.setTextSize(50);

        //Scaling Text
        text2 = new Text(this);
        text2.setText("当前缩放倍数: x" + df.format(mFactor));
        text2.setTextSize(50);

        directionalLayout.setBackground(background);
        directionalLayout.setAlignment(LayoutAlignment.HORIZONTAL_CENTER);
        directionalLayout.setLayoutConfig(layoutConfig);
        directionalLayout.addComponent(uCropView);
        directionalLayout.addComponent(rotationController);
        directionalLayout.addComponent(text);
        directionalLayout.addComponent(scaleController);
        directionalLayout.addComponent(text2);
        //Cut button
        directionalLayout.addComponent(button);
        setUIContent(directionalLayout);
    }

    protected void cropAndSaveImage() {
        mGestureCropImageView = uCropView.getCropImageView();
        mGestureCropImageView.cropAndSaveImage("image/jpg", 90, null);

        Intent intent = new Intent();
        getAbility().setResult(Boxing.CROP_RESULT_CODE, intent);
        getAbility().terminateAbility();
    }

    private Uri saveImage(String fileName, PixelMap pixelMap) {
        try {
            ValuesBucket valuesBucket = new ValuesBucket();
            valuesBucket.putString(AVStorage.Images.Media.DISPLAY_NAME, fileName);
            valuesBucket.putString("relative_path", "DCIM/");
            valuesBucket.putString(AVStorage.Images.Media.MIME_TYPE, "image/JPEG");
            //Application exclusive
            valuesBucket.putInteger("is_pending", 1);
            DataAbilityHelper helper = DataAbilityHelper.creator(this);
            int id = helper.insert(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, valuesBucket);
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
            //Here you need & quot;w" write permissions
            FileDescriptor fd = helper.openFile(uri, "w");
            ImagePacker imagePacker = ImagePacker.create();
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            OutputStream outputStream = new FileOutputStream(fd);

            packingOptions.format = "image/jpeg";
            packingOptions.quality = 90;
            boolean result = imagePacker.initializePacking(outputStream, packingOptions);
            if (result) {
                result = imagePacker.addImage(pixelMap);
                if (result) {
                    long dataSize = imagePacker.finalizePacking();
                }
            }
            outputStream.flush();
            outputStream.close();
            valuesBucket.clear();
            //Release of exclusivity
            valuesBucket.putInteger("is_pending", 0);
            helper.update(uri, valuesBucket, null);
            return uri;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
