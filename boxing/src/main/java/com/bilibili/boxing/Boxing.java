/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing;

import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.ResultMedia;
import com.bilibili.boxing.presenter.PickerPresenter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * An entry for {@link AbsBoxingActivity} and {@link AbsBoxingViewFragment}.<br/>
 * 1.call {@link #of(BoxingConfig)} to pick a mode.<br/>
 * 2.to use {@link AbsBoxingActivity} + {@link AbsBoxingViewFragment} combination,
 * call {@link #withIntent(Context, Class)} to make a intent and {@link #start(Ability)} to start a new Activity.<br/>
 * to use {@link AbsBoxingViewFragment} only, just call {@link #setupFragment(AbsBoxingViewFragment, OnBoxingFinishListener)}.<br/>
 * 3 4.to get result from a new Activity, call {@link #getResult(Intent)} in {@lin Ability#onAbilityResult(int, int, Intent)} .
 */
public class Boxing {
    public static final String EXTRA_BUNDLENAME = "com.bilibili.boxing.Boxing.bundleName";
    public static final String EXTRA_SELECTED_MEDIA = "com.bilibili.boxing.Boxing.selected_media";
    public static final String EXTRA_ALBUM_ID = "com.bilibili.boxing.Boxing.album_id";

    static final String EXTRA_CONFIG = "com.bilibili.boxing.Boxing.config";
    public static final String EXTRA_RESULT = "com.bilibili.boxing.Boxing.result";
    static final String EXTRA_START_POS = "com.bilibili.boxing.Boxing.start_pos";
    public static final String EXTRA_RESULT_ID = "com.bilibili.boxing.Boxing.result_id";
    public static final String EXTRA_RESULT_CAMERA = "com.bilibili.boxing.Boxing.result_camera";
    public static final String EXTRA_RESULT_VIDEO_ID = "com.bilibili.boxing.Boxing.result_video_id";

    public static final int COMPRESS_RESULT_CODE = 0;
    public static final int CAMERA_RESULT_CODE = 1;
    public static final int CROP_RESULT_CODE = 2;
    public static final int CROP_RESULT_CODE2 = 3;

    private Intent mIntent;

    private Boxing(BoxingConfig config) {
        BoxingManager.getInstance().setBoxingConfig(config);
        this.mIntent = new Intent();
    }

    /**
     * get the media result.
     */
    @Nullable
    public static ArrayList<BaseMedia> getResult(Intent data) {
        if (data != null) {
            return data.getSequenceableArrayListParam(EXTRA_RESULT);
        }
        return null;
    }

    /**
     * call {@link #of(BoxingConfig)} first to specify the mode otherwise {@link BoxingConfig.Mode#MULTI_IMG} is used.<br/>
     */
    public static Boxing get() {
        BoxingConfig config = BoxingManager.getInstance().getBoxingConfig();
        if (config == null) {
            config = new BoxingConfig(BoxingConfig.Mode.MULTI_IMG).needGif();
            BoxingManager.getInstance().setBoxingConfig(config);
        }
        return new Boxing(config);
    }

    /**
     * create a boxing entry.
     *
     * @param config {@link BoxingConfig}
     */
    public static Boxing of(BoxingConfig config) {
        return new Boxing(config);
    }

    /**
     * create a boxing entry.
     *
     * @param mode {@link BoxingConfig.Mode}
     */
    public static Boxing of(BoxingConfig.Mode mode) {
        return new Boxing(new BoxingConfig(mode));
    }

    /**
     * create a boxing entry. use {@link BoxingConfig.Mode#MULTI_IMG}.
     */
    public static Boxing of() {
        BoxingConfig config = new BoxingConfig(BoxingConfig.Mode.MULTI_IMG).needGif();
        return new Boxing(config);
    }

    /**
     * get the intent build by boxing after call {@link #withIntent}.
     */
    public Intent getIntent() {
        return mIntent;
    }

    /**
     * same as {@link Intent#"setClass"(Context, Class)}
     */
    public Boxing withIntent(Context context, Class<?> cls) {
        return withIntent(context, cls, null);
    }

    /**
     * {@link Intent#"setClass"(Context, Class)} with input medias.
     */
    public Boxing withIntent(Context context, Class<?> cls, ArrayList<? extends BaseMedia> selectedMedias) {
        ArrayList<ResultMedia> list = new ArrayList<>();
        if (selectedMedias != null && !selectedMedias.isEmpty()) {
            for (int i = 0; i < selectedMedias.size(); i++) {
                ResultMedia resultMedia = new ResultMedia();
                resultMedia.setId(selectedMedias.get(i).getId());
                list.add(resultMedia);
            }
            mIntent.setSequenceableArrayListParam(EXTRA_SELECTED_MEDIA, list);
        }
        return this;
    }

    /**
     * use to start image viewer.
     *
     * @param medias selected medias.
     * @param pos    the start position.
     */
    public Boxing withIntent(Context context, Class<?> cls, ArrayList<? extends BaseMedia> medias, int pos) {
        withIntent(context, cls, medias, pos, "");
        return this;
    }


    /**
     * use to start image viewer.
     *
     * @param medias  selected medias.
     * @param pos     the start position.
     * @param albumId the specify album id.
     */
    public Boxing withIntent(Context context, Class<?> cls, ArrayList<? extends BaseMedia> medias, int pos, String albumId) {
        if (medias != null && !medias.isEmpty()) {
            mIntent.setParam(EXTRA_SELECTED_MEDIA, medias);
        }
        if (pos >= 0) {
            mIntent.setParam(EXTRA_START_POS, pos);
        }
        if (albumId != null) {
            mIntent.setParam(EXTRA_ALBUM_ID, albumId);
        }
        return this;
    }

    /**
     * same as {@link Ability#startAbility(Intent)}
     */
    public void start(Ability activity) {
        activity.startAbility(mIntent);
    }

    /**
     * use to start raw image viewer.
     *
     * @param viewMode {@link BoxingConfig.ViewMode}
     */
    public void start(Ability activity, BoxingConfig.ViewMode viewMode) {
        BoxingManager.getInstance().getBoxingConfig().withViewer(viewMode);
        activity.startAbility(mIntent);
    }

    /**
     * same as {@link Ability#startAbilityForResult(Intent, int)}
     */
    public void start(Ability activity, int requestCode, String bundleName, String abilityName) {
        Intent intent1 = new Intent();
        intent1.setParam(Boxing.EXTRA_BUNDLENAME, bundleName);
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(bundleName)
                .withAbilityName(abilityName)
                .build();
        intent1.setOperation(operation);
        activity.startAbilityForResult(intent1, requestCode);
    }

    /**
     * same as {@link Fraction#startAbility(Intent, int)}
     */
    public void start(Fraction fragment, int requestCode) {
        fragment.startAbility(mIntent, requestCode);
    }

    /**
     * use to start raw image viewer.
     *
     * @param viewMode {@link BoxingConfig.ViewMode}
     */
    public void start(Fraction fragment, int requestCode, BoxingConfig.ViewMode viewMode, String bundleName, String abilityName) {
        BoxingManager.getInstance().getBoxingConfig().withViewer(viewMode);

        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(bundleName)
                .withAbilityName(abilityName)
                .build();
        mIntent.setOperation(operation);
        fragment.getFractionAbility().startAbilityForResult(mIntent, requestCode);
    }

    /**
     * set up a subclass of {@link AbsBoxingViewFragment} without a {@link AbsBoxingActivity}.
     *
     * @param fragment         subclass of {@link AbsBoxingViewFragment}
     * @param onFinishListener a listener fo media result
     */
    public void setupFragment(AbsBoxingViewFragment fragment, OnBoxingFinishListener onFinishListener) {
        PickerPresenter pickerPresenter = new PickerPresenter(fragment);
        fragment.setPresenter(pickerPresenter);
        fragment.setOnFinishListener(onFinishListener);
    }

    /**
     * work with a subclass of {@link AbsBoxingViewFragment} without a {@link AbsBoxingActivity}.
     */
    public interface OnBoxingFinishListener {

        /**
         * live with {@link com.bilibili.boxing.presenter.PickerContract.View#onFinish(List)}
         *
         * @param medias the selection of medias.
         */
        void onBoxingFinish(Intent intent, List<BaseMedia> medias);
    }

}
