/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.utils;


import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;


public class BoxingExecutor {
    private static final BoxingExecutor INSTANCE = new BoxingExecutor();

    private ExecutorService mExecutorService;

    private BoxingExecutor() {
    }

    public static BoxingExecutor getInstance() {
        return INSTANCE;
    }

    public void runWorker(Runnable runnable) {
        ensureWorkerHandlerNotNull();
        try {
            mExecutorService.execute(runnable);
        } catch (Exception e) {
            BoxingLog.d("runnable stop running unexpected. " + e.getMessage());
        }
    }

    @Nullable
    public FutureTask<Boolean> runWorker(Callable<Boolean> callable) {
        ensureWorkerHandlerNotNull();
        FutureTask<Boolean> task = null;
        try {
            task = new FutureTask<>(callable);
            mExecutorService.submit(task);
            return task;
        } catch (Exception e) {
            BoxingLog.d("callable stop running unexpected. " + e.getMessage());
        }
        return task;
    }

    public void runUI(Runnable runnable) {
        if (EventRunner.current() == EventRunner.getMainEventRunner()) {
            runnable.run();
            return;
        }
        EventHandler handler = ensureUiHandlerNotNull();
        try {
            handler.postTask(runnable);
        } catch (Exception e) {
            BoxingLog.d("update UI task fail. " + e.getMessage());
        }
    }

    private void ensureWorkerHandlerNotNull() {
        if (mExecutorService == null) {
            mExecutorService = Executors.newCachedThreadPool();
        }
    }

    private EventHandler ensureUiHandlerNotNull() {
        return new EventHandler(EventRunner.create());
    }

}
