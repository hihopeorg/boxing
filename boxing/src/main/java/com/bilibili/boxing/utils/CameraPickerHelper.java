/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.utils;

import com.bilibili.boxing.AbsBoxingViewFragment;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Matrix;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * A helper to start camera.<br/>
 * used by {@link AbsBoxingViewFragment}
 */
public class CameraPickerHelper {
    private static final int MAX_CAMER_PHOTO_SIZE = 4 * 1024 * 1024;
    public static final int REQ_CODE_CAMERA = 0x2001;
    private static final String STATE_SAVED_KEY = "com.bilibili.boxing.utils.CameraPickerHelper.saved_state";

    private String mSourceFilePath;
    private File mOutputFile;
    private Callback mCallback;

    public interface Callback {
        void onFinish(CameraPickerHelper helper);

        void onError(CameraPickerHelper helper);
    }

    public CameraPickerHelper(Intent savedInstance) {
        if (savedInstance != null) {
            SavedState state = savedInstance.getSequenceableParam(STATE_SAVED_KEY);
            if (state != null) {
                mOutputFile = state.mOutputFile;
                mSourceFilePath = state.mSourceFilePath;
            }
        }
    }

    public void setPickCallback(Callback callback) {
        this.mCallback = callback;
    }

    public String getSourceFilePath() {
        return mSourceFilePath;
    }

    /**
     * deal with the system camera's shot.
     */
    public boolean onActivityResult(final int requestCode, final int resultCode) {
        if (requestCode != REQ_CODE_CAMERA) {
            return false;
        }
        try {
            return rotateImage(resultCode);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean rotateSourceFile(File file) throws IOException {
        if (file == null || !file.exists()) {
            return false;
        }
        FileOutputStream outputStream = null;
        PixelMap bitmap = null;
        PixelMap outBitmap = null;
        try {
            int degree = BoxingExifHelper.getRotateDegree(file.getAbsolutePath());
            if (degree == 0) {
                return true;
            }
            int quality = file.length() >= MAX_CAMER_PHOTO_SIZE ? 90 : 100;
            Matrix matrix = new Matrix();
            matrix.postRotate(degree);
            ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
            ImageSource imageSource = ImageSource.create(file.getAbsolutePath(), new ImageSource.SourceOptions());
            options.allowPartialImage = false;
            bitmap = imageSource.createPixelmap(options);
            outBitmap = PixelMap.create(bitmap, new PixelMap.InitializationOptions());
            outputStream = new FileOutputStream(file);
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.quality = quality;
            ImagePacker.create().initializePacking(outputStream, packingOptions);
            outputStream.flush();
            return true;
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    BoxingLog.d("IOException when output stream closing!");
                }
            }
            if (bitmap != null) {
                bitmap.release();
            }
            if (outBitmap != null) {
                outBitmap.release();
            }
        }
    }

    private boolean rotateImage(int resultCode) throws IOException {
        return rotateSourceFile(mOutputFile);
    }

    public void release() {
        mOutputFile = null;
    }

    private static class SavedState implements Sequenceable {
        private File mOutputFile;
        private String mSourceFilePath;

        SavedState() {
        }

        @Override
        public boolean marshalling(Parcel parcel) {
            parcel.writeSerializable(this.mOutputFile);
            return parcel.writeString(this.mSourceFilePath);
        }

        @Override
        public boolean unmarshalling(Parcel parcel) {
            this.mOutputFile = parcel.readSerializable(File.class);
            this.mSourceFilePath = parcel.readString();
            return true;
        }

        public static final Sequenceable.Producer
                PRODUCER = new Sequenceable.Producer
                () {
            public SavedState createFromParcel(Parcel in) {
                // Initialize an instance first, then do customized unmarshlling.
                SavedState instance = new SavedState();
                instance.unmarshalling(in);
                return instance;
            }
        };
    }

}
