/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing.utils;


import ohos.media.image.ImageSource;
import ohos.media.image.PropertyFilter;
import ohos.media.image.common.PropertyKey;

import java.io.IOException;

public class BoxingExifHelper {

    public static void removeExif(String path) {
        if (!path.isEmpty()) {
            return;
        }
        PropertyFilter exifInterface;
        exifInterface = new PropertyFilter();
        ImageSource imageSource = ImageSource.create(path, new ImageSource.SourceOptions());
        try {
            exifInterface.applyToSource(imageSource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        exifInterface.setPropertyString(PropertyKey.Exif.ARTIST, "");
        exifInterface.setPropertyString(PropertyKey.Exif.RESOLUTION_UNIT, "0");
        exifInterface.setPropertyString(PropertyKey.Exif.DATETIME_ORIGINAL, "");
        exifInterface.setPropertyString(PropertyKey.Exif.MAKER_NOTE, "0");
        exifInterface.setPropertyString(PropertyKey.Exif.DATETIME_DIGITIZED, "");
        exifInterface.setPropertyString(PropertyKey.Exif.MAKE, "");
        exifInterface.setPropertyString(PropertyKey.Exif.MODEL, "");
        exifInterface.setPropertyString(PropertyKey.Exif.ORIENTATION, "0");

        exifInterface.setPropertyString(PropertyKey.Exif.DATETIME, "");
        exifInterface.setPropertyString(PropertyKey.Exif.GPS_LATITUDE, "");
        exifInterface.setPropertyString(PropertyKey.Exif.GPS_LONGITUDE, "");

        exifInterface.setPropertyString(PropertyKey.Exif.GPS_LATITUDE, "");
    }

    static int getRotateDegree(String path) {
        return 180;
    }

}
