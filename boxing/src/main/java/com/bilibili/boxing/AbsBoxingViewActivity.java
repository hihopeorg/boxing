/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing;

import com.bilibili.boxing.loader.IBoxingCallback;
import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.AlbumEntity;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.ResultMedia;
import com.bilibili.boxing.presenter.PickerContract;
import com.bilibili.boxing.presenter.PickerPresenter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * A abstract class which implements {@link PickerContract.View} for custom media view.
 * For view big images.
 */
public abstract class AbsBoxingViewActivity extends Ability implements PickerContract.View {
    ArrayList<BaseMedia> mSelectedImages = new ArrayList<>();
    ArrayList<ResultMedia> mSelectedImagesResult;
    String mAlbumId;
    int mStartPos;
    private PickerContract.Presenter mPresenter;

    /**
     * start loading when the permission request is completed.
     * call {@link #loadMedias()} or {@link #loadMedias(int, String)}.
     */
    public abstract void startLoading();

    /**
     * override this method to handle the medias.
     * make sure {@link #loadMedias()} ()} being called first.
     *
     * @param medias the results of medias
     */
    @Override
    public void showMedia(List<BaseMedia> medias, int allCount) {
    }

    @Override
    public void showAlbum(List<AlbumEntity> albums) {
    }

    /**
     * to clear all medias the first time(the page number is 0). do some clean work.
     */
    @Override
    public void clearMedia() {
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        BoxingConfig config;
        if (intent != null) {
            config = intent.getSequenceableParam(Boxing.EXTRA_CONFIG);
        } else {
            config = BoxingManager.getInstance().getBoxingConfig();
        }
        setPickerConfig(config);
        parseSelectedMedias(intent, getIntent());
        setPresenter(new PickerPresenter(this));
    }

    private void parseSelectedMedias(Intent savedInstanceState, Intent intent) {
        if (savedInstanceState != null) {
            mSelectedImages = savedInstanceState.getSequenceableArrayListParam(Boxing.EXTRA_SELECTED_MEDIA);
            mAlbumId = savedInstanceState.getStringParam(Boxing.EXTRA_ALBUM_ID);
            mStartPos = savedInstanceState.getIntParam(Boxing.EXTRA_START_POS, 0);
        } else if (intent != null) {
            mStartPos = intent.getIntParam(Boxing.EXTRA_START_POS, 0);
            mSelectedImages = intent.getSequenceableArrayListParam(Boxing.EXTRA_SELECTED_MEDIA);
            mAlbumId = intent.getStringParam(Boxing.EXTRA_ALBUM_ID);
        }
        mSelectedImagesResult = intent.getSequenceableArrayListParam(Boxing.EXTRA_SELECTED_MEDIA);
    }

    @Override
    public final void setPresenter(PickerContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    /**
     * get the {@link DataAbilityHelper}
     */
    @Override
    public final DataAbilityHelper getAppCr() {
        return DataAbilityHelper.creator(getApplicationContext());
    }

    public final void loadRawImage(Image img, String path, int width, int height, IBoxingCallback callback) {
        BoxingMediaLoader.getInstance().displayRaw(img, path, width, height, callback);
    }

    /**
     * called the job is done.Click the ok button, take a photo from camera, crop a photo.
     * most of the time, you do not have to override.
     *
     * @param medias the list of selection
     */
    @Override
    public void onFinish(List<BaseMedia> medias) {
        Intent intent = new Intent();
        intent.setParam(Boxing.EXTRA_RESULT, (ArrayList<BaseMedia>) medias);
    }

    /**
     * need crop or not
     *
     * @return true, need it.
     */
    public final boolean hasCropBehavior() {
        BoxingConfig config = BoxingManager.getInstance().getBoxingConfig();
        return config != null && config.isSingleImageMode() && config.getCropOption() != null;
    }

    /**
     * to start the crop behavior, call it when {@link #hasCropBehavior()} return true.
     *
     * @param media       the media to be cropped.
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     */
    @Override
    public final void startCrop(BaseMedia media, int requestCode) {
    }

    /**
     * set or update the config.most of the time, you do not have to call it.
     *
     * @param config {@link BoxingConfig}
     */
    @Override
    public final void setPickerConfig(BoxingConfig config) {
        if (config == null) {
            return;
        }
        BoxingManager.getInstance().setBoxingConfig(config);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        getIntent().setParam(Boxing.EXTRA_CONFIG, BoxingManager.getInstance().getBoxingConfig());
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);

    }

    /**
     * call this to clear resource.
     */
    @Override
    protected void onStop() {
        super.onStop();
        if (mPresenter != null) {
            mPresenter.destroy();
        }
    }

    /**
     * in {@link BoxingConfig.Mode#MULTI_IMG}, call this to pick the selected medias in all medias.
     */
    public final void checkSelectedMedia(List<BaseMedia> allMedias, List<BaseMedia> selectedMedias) {
        mPresenter.checkSelectedMedia(allMedias, selectedMedias);
    }

    /**
     * load first page of medias.
     * use {@link #showMedia(List, int)} to get the result.
     */
    public final void loadMedias() {
        mPresenter.loadMedias(0, AlbumEntity.DEFAULT_NAME);
    }

    /**
     * load the medias for the specify page and album id.
     * use {@link #showMedia(List, int)} to get the result.
     *
     * @param page    page numbers.
     * @param albumId the album id is {@link AlbumEntity#mBucketId}.
     */
    public final void loadMedias(int page, String albumId) {
        mPresenter.loadMedias(page, albumId);
    }

    /**
     * get the max count set before
     */
    public final int getMaxCount() {
        BoxingConfig config = BoxingManager.getInstance().getBoxingConfig();
        if (config == null) {
            return BoxingConfig.DEFAULT_SELECTED_COUNT;
        }
        return config.getMaxCount();
    }

    public final ArrayList<BaseMedia> getSelectedImages() {
        if (mSelectedImages != null) {
            return mSelectedImages;
        }
        return new ArrayList<>();
    }

    public final ArrayList<ResultMedia> getSelectedImagesResult() {
        if (mSelectedImagesResult != null) {
            return mSelectedImagesResult;
        }
        return new ArrayList<>();
    }

    public final String getAlbumId() {
        return mAlbumId;
    }

    public final int getStartPos() {
        return mStartPos;
    }
}
