/*
 *    Copyright 2017, Yalantis
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yalantis.ucrop.view;

import com.yalantis.ucrop.ResourceTable;
import com.yalantis.ucrop.callback.CropBoundsChangeListener;
import com.yalantis.ucrop.callback.OverlayViewChangeListener;
import com.yalantis.ucrop.util.Config;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;


public class UCropView extends StackLayout {

    StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);

    private GestureCropImageView mGestureCropImageView;
    private final OverlayView mViewOverlay;

    public UCropView(Context context) {
        this(context, null);
    }

    public UCropView(Context context, AttrSet attrs) {
        this(context, attrs, "0");
    }

    public UCropView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setLayoutConfig(layoutConfig);

        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_ucrop_view, this, true);
        mGestureCropImageView = (GestureCropImageView) findComponentById(ResourceTable.Id_image_view_crop);
        mViewOverlay = (OverlayView) findComponentById(ResourceTable.Id_view_overlay);

        Config a = new Config();
        mViewOverlay.processStyledAttributes(a);
        mGestureCropImageView.processStyledAttributes(a);

        setListenersToViews();
    }

    private void setListenersToViews() {
        mGestureCropImageView.setCropBoundsChangeListener(new CropBoundsChangeListener() {
            @Override
            public void onCropAspectRatioChanged(float cropRatio) {
                mViewOverlay.setTargetAspectRatio(cropRatio);
            }
        });
        mViewOverlay.setOverlayViewChangeListener(new OverlayViewChangeListener() {
            @Override
            public void onCropRectUpdated(RectFloat cropRect) {
                mGestureCropImageView.setCropRect(cropRect);
            }
        });
    }

    public GestureCropImageView getCropImageView() {
        return mGestureCropImageView;
    }


    public OverlayView getOverlayView() {
        return mViewOverlay;
    }

    /**
     * Method for reset state for UCropImageView such as rotation, scale, translation.
     * Be careful: this method recreate UCropImageView instance and reattach it to layout.
     */
    public void resetCropImageView() {
        removeComponent(mGestureCropImageView);
        mGestureCropImageView = new GestureCropImageView(getContext());
        setListenersToViews();
        mGestureCropImageView.setCropRect(getOverlayView().getCropViewRect());
        addComponent(mGestureCropImageView, 0);
    }
}