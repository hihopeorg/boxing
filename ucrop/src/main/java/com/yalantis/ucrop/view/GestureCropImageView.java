/*
 *    Copyright 2017, Yalantis
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yalantis.ucrop.view;


import com.yalantis.ucrop.util.RotationGestureDetector;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ScaleInfo;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by Oleksii Shliama (https://github.com/shliama).
 */
public class GestureCropImageView extends CropImageView implements Component.TouchEventListener {

    private RotationGestureDetector mRotateDetector;
    ScaleListener mScaledListener;
    ScrolledListener mScrolledListener;
    private float before_X = -1;
    private float before_Y = -1;

    private float mMidPntX, mMidPntY;

    private boolean mIsRotateEnabled = true;
    private boolean mIsScaleEnabled = true;
    private int mDoubleTapScaleSteps = 5;

    public GestureCropImageView(Context context) {
        super(context);
    }

    public GestureCropImageView(Context context, AttrSet attrs) {
        this(context, attrs, "0");
    }

    public GestureCropImageView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
    }

    public void setScaleEnabled(boolean scaleEnabled) {
        mIsScaleEnabled = scaleEnabled;
    }

    public boolean isScaleEnabled() {
        return mIsScaleEnabled;
    }

    public void setRotateEnabled(boolean rotateEnabled) {
        mIsRotateEnabled = rotateEnabled;
    }

    public boolean isRotateEnabled() {
        return mIsRotateEnabled;
    }

    public void setDoubleTapScaleSteps(int doubleTapScaleSteps) {
        mDoubleTapScaleSteps = doubleTapScaleSteps;
    }

    public int getDoubleTapScaleSteps() {
        return mDoubleTapScaleSteps;
    }

    @Override
    protected void init() {
        super.init();
        setupGestureListeners();
    }

    /**
     * This method calculates target scale value for double tap gesture.
     * User is able to zoom the image from min scale value
     * to the max scale value with {@link #mDoubleTapScaleSteps} double taps.
     */
    protected float getDoubleTapTargetScale() {
        return getCurrentScale() * (float) Math.pow(getMaxScale() / getMinScale(), 1.0f / mDoubleTapScaleSteps);
    }

    private void setupGestureListeners() {

        //旋转监听
        mRotateDetector = new RotationGestureDetector(new RotateListener());

        //触摸监听
        setTouchEventListener(this);
        //缩放监听
        mScaledListener = new ScaleListener();
        setScaledListener(mScaledListener);
    }

    //由于鸿蒙滑动监听缺陷，所以滑动操作整合在触摸监听里
    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if ((event.getAction()) == TouchEvent.PRIMARY_POINT_DOWN) {
            cancelAllAnimations();
        }

        if (event.getPointerCount() > 1) {
            //获取缩放中点
            mMidPntX = (event.getPointerPosition(0).getX() + event.getPointerPosition(1).getX()) / 2;
            mMidPntY = (event.getPointerPosition(0).getY() + event.getPointerPosition(1).getY()) / 2;
        }
        if (event.getPointerCount() == 1) {
            //滑动
            float now_X = event.getPointerPosition(0).getX();
            float now_Y = event.getPointerPosition(0).getY();
            if (before_X > 0 && before_Y > 0) {
                float scroll_X = now_X - before_X;
                float scroll_Y = now_Y - before_Y;
                postTranslate(scroll_X, scroll_Y);
            }
            before_X = now_X;
            before_Y = now_Y;
            if (event.getAction() == TouchEvent.PRIMARY_POINT_UP) {
                before_X = -1;
                before_Y = -1;
            }
        }

        if (mIsRotateEnabled) {
            mRotateDetector.onTouchEvent(event);
        }

        if ((event.getAction()) == TouchEvent.PRIMARY_POINT_UP) {
            setImageToWrapCropBounds();
        }
        return true;
    }

    private class ScaleListener implements ScaledListener {


        @Override
        public void onScaleStart(Component component, ScaleInfo scaleInfo) {

        }

        @Override
        public void onScaleUpdate(Component component, ScaleInfo scaleInfo) {
            mMidPntX = scaleInfo.updatePoint.position[0];
            mMidPntY = scaleInfo.updatePoint.position[1];
            postScale((float) (0.1f * scaleInfo.scale), mMidPntX, mMidPntY);
        }

        @Override
        public void onScaleEnd(Component component, ScaleInfo scaleInfo) {

        }
    }

    private class RotateListener extends RotationGestureDetector.SimpleOnRotationGestureListener {

        @Override
        public boolean onRotation(RotationGestureDetector rotationDetector) {
            postRotate(rotationDetector.getAngle(), mMidPntX, mMidPntY);
            return true;
        }

    }

}
