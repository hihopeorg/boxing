/*
 *    Copyright 2017, Yalantis
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yalantis.ucrop.task;

import com.yalantis.ucrop.callback.BitmapLoadCallback;
import com.yalantis.ucrop.model.ExifInfo;
import com.yalantis.ucrop.util.BitmapLoadUtils;
import com.yalantis.ucrop.util.LogUtils;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.utils.Matrix;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.utils.net.Uri;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSource;
import okio.Okio;
import okio.Sink;

import java.io.*;


/**
 * Creates and returns a Bitmap for a given Uri(String url).
 * inSampleSize is calculated based on requiredWidth property. However can be adjusted if OOM occurs.
 * If any EXIF config is found - bitmap is transformed properly.
 */
public class BitmapLoadTask {

    private static final String TAG = "BitmapWorkerTask";

    private static final int MAX_BITMAP_SIZE = 100 * 1024 * 1024;   // 100 MB

    private final Context mContext;
    private Uri mInputUri;
    private Uri mOutputUri;
    private final int mRequiredWidth;
    private final int mRequiredHeight;

    private final BitmapLoadCallback mBitmapLoadCallback;

    //原BitmapWorkerResult内的参数
    PixelMap mBitmapResult;
    ExifInfo mExifInfo;
    Exception mBitmapWorkerException;


    public BitmapLoadTask(Context context,
                          Uri inputUri, Uri outputUri,
                          int requiredWidth, int requiredHeight,
                          BitmapLoadCallback loadCallback) {
        mContext = context;
        mInputUri = inputUri;
        mOutputUri = outputUri;
        mRequiredWidth = requiredWidth;
        mRequiredHeight = requiredHeight;
        mBitmapLoadCallback = loadCallback;
    }

    //原AsyncTask的doInBackground
    public void doInBackground() {
        EventRunner eventRunner = EventRunner.create();
        EventHandler handler = new EventHandler(eventRunner);
        handler.postTask(new Runnable() {
            @Override
            public void run() {
                if (mInputUri == null) {
                    mBitmapWorkerException = new NullPointerException("Input Uri cannot be null");
                    mBitmapLoadCallback.onFailure(mBitmapWorkerException);
                    return;
                }

                try {
                    processInputUri();
                } catch (NullPointerException | IOException e) {
                    mBitmapWorkerException = e;
                    mBitmapLoadCallback.onFailure(mBitmapWorkerException);
                    return;
                }
                //放在这里有用吗,移动到解码之后？
                final ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
//                options.sampleSize = BitmapLoadUtils.calculateInSampleSize(options, mRequiredWidth, mRequiredHeight);

                PixelMap decodeSampledBitmap = null;

                boolean decodeAttemptSuccess = false;
                while (!decodeAttemptSuccess) {
                    try {
                        //安卓通过InputUri获取InputStream
                        //InputStream stream = mContext.getContentResolver().openInputStream(mInputUri);
                        //鸿蒙通过InputUri获取InputStream
                        InputStream stream = null;
                        DataAbilityHelper helper = DataAbilityHelper.creator(mContext);
                        FileDescriptor fd = helper.openFile(mInputUri, "r");
                        stream = new FileInputStream(fd);
                        LogUtils.LogInfo("qqqqqq:", String.valueOf(mInputUri));
                        LogUtils.LogInfo("qqqqqq:", String.valueOf(stream));
//                        HttpURLConnection connection = null;
//                        URL url = new URL(String.valueOf(mInputUri));
//                        URLConnection urlConnection = url.openConnection();
//
//                        if (urlConnection instanceof HttpURLConnection) {
//                            connection = (HttpURLConnection) urlConnection;
//                        }
//                        if (connection != null) {
//                            connection.connect();
//                            // 之后可进行url的其他操作
//                            // 得到服务器返回过来的流对象
//                            stream = urlConnection.getInputStream();
//                        }
                        try {
                            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
                            ImageSource imageSource = ImageSource.create(stream, sourceOptions);
                            //一次读取获取原图大小
                            decodeSampledBitmap = imageSource.createPixelmap(options);
                            Size rawSize = decodeSampledBitmap.getImageInfo().size;
                            LogUtils.LogInfo("qqqqqq:", "高" + rawSize.height + "宽" + rawSize.width);
                            options.sampleSize = BitmapLoadUtils.calculateInSampleSize(rawSize, mRequiredWidth, mRequiredHeight);
                            options.desiredSize = new Size(rawSize.width / options.sampleSize, rawSize.height / options.sampleSize);
                            LogUtils.LogInfo("qqqqqq:", "sample size" + options.sampleSize);
                            //二次读取，获取缩放后的图像
                            decodeSampledBitmap = imageSource.createPixelmap(options);
                            if (decodeSampledBitmap == null) {
                                mBitmapWorkerException = new IllegalArgumentException("bitmap could not be retrieved from the Uri: [" + mInputUri + "]");
                                mBitmapLoadCallback.onFailure(mBitmapWorkerException);
                                return;

                            }
                        } finally {
                            BitmapLoadUtils.close(stream);
                        }
                        if (checkSize(decodeSampledBitmap, options)) continue;
                        decodeAttemptSuccess = true;
                    } catch (OutOfMemoryError error) {
                        LogUtils.LogError(TAG, "doInBackground: BitmapFactory.decodeFileDescriptor: " + error);
                        options.sampleSize *= 2;
                    } catch (IOException e) {
                        LogUtils.LogError(TAG, "doInBackground: ImageDecoder.createSource: " + e);
                        mBitmapWorkerException = new IllegalArgumentException("PixelMap could not be decoded from the Uri: [" + mInputUri + "]", e);
                        mBitmapLoadCallback.onFailure(mBitmapWorkerException);
                        return;
                    } catch (DataAbilityRemoteException e) {
                        e.printStackTrace();
                    } catch (RuntimeException e) {
                    }
                }

                if (decodeSampledBitmap == null) {
                    mBitmapWorkerException = new IllegalArgumentException("PixelMap could not be decoded from the Uri: [" + mInputUri + "]");
                    mBitmapLoadCallback.onFailure(mBitmapWorkerException);
                    return;
                }

                int exifOrientation = BitmapLoadUtils.getExifOrientation(mContext, mInputUri);
                int exifDegrees = BitmapLoadUtils.exifToDegrees(exifOrientation);
                int exifTranslation = BitmapLoadUtils.exifToTranslation(exifOrientation);

                ExifInfo exifInfo = new ExifInfo(exifOrientation, exifDegrees, exifTranslation);

                Matrix matrix = new Matrix();
                if (exifDegrees != 0) {
                    matrix.preRotate(exifDegrees);
                }
                if (exifTranslation != 1) {
                    matrix.postScale(exifTranslation, 1);
                }
                if (!matrix.isIdentity()) {
                    BitmapLoadUtils bitmapLoadUtils = new BitmapLoadUtils();
                    mBitmapResult = bitmapLoadUtils.transformBitmap(decodeSampledBitmap, exifDegrees, exifTranslation, mContext);
                    mExifInfo = exifInfo;
                    mBitmapLoadCallback.onBitmapLoaded(mBitmapResult, mExifInfo, mInputUri.getDecodedPath(), (mOutputUri == null) ? null : mOutputUri.getDecodedPath());
                    return;
                }


                mBitmapResult = decodeSampledBitmap;
                LogUtils.LogInfo("qqqqqqqq:", String.valueOf(mBitmapResult.getImageInfo().size.height));
                mExifInfo = exifInfo;
                mBitmapLoadCallback.onBitmapLoaded(mBitmapResult, mExifInfo, mInputUri.getDecodedPath(), (mOutputUri == null) ? null : mOutputUri.getDecodedPath());
                LogUtils.LogInfo("qqqqqq:", "main done");
                return;
            }
        });

    }


    private void processInputUri() throws NullPointerException, IOException {
        String inputUriScheme = mInputUri.getScheme();
        if ("http".equals(inputUriScheme) || "https".equals(inputUriScheme)) {
            try {
                downloadFile(mInputUri, mOutputUri);
            } catch (NullPointerException e) {
                LogUtils.LogError(TAG, "Downloading failed:" + e);
                throw e;
            }
        } else if ("content".equals(inputUriScheme)) {
            try {
                copyFile(mInputUri, mOutputUri);
            } catch (NullPointerException | IOException e) {
                LogUtils.LogError(TAG, "Copying failed:" + e);
                throw e;
            }
        } else if ("dataability".equals(inputUriScheme)) {
            try {
                copyFile(mInputUri, mOutputUri);
            } catch (NullPointerException | IOException e) {
                LogUtils.LogError(TAG, "Copying failed:" + e);
                throw e;
            }
        } else if (!"file".equals(inputUriScheme)) {
            LogUtils.LogError(TAG, "Invalid Uri scheme " + inputUriScheme);
            throw new IllegalArgumentException("Invalid Uri scheme" + inputUriScheme);
        }
    }

    private void copyFile(Uri inputUri, Uri outputUri) throws NullPointerException, IOException {
        LogUtils.LogDebug(TAG, "copyFile");

        if (outputUri == null) {
            throw new NullPointerException("Output Uri is null - cannot copy image");
        }

        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            //安卓通过InputUri获取InputStream
            //inputStream = mContext.getContentResolver().openInputStream(inputUri);
            //鸿蒙通过InputUri获取InputStream
            DataAbilityHelper helper = DataAbilityHelper.creator(mContext);
            FileDescriptor fd_i = helper.openFile(mInputUri, "r");
            inputStream = new FileInputStream(fd_i);
            FileDescriptor fd_o = helper.openFile(outputUri, "w");
            outputStream = new FileOutputStream(fd_o);
            if (inputStream == null) {
                throw new NullPointerException("InputStream for given input Uri is null");
            }

            byte buffer[] = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } finally {
            BitmapLoadUtils.close(outputStream);
            BitmapLoadUtils.close(inputStream);

            // swap uris, because input image was copied to the output destination
            // (cropped image will override it later)
            mInputUri = mOutputUri;
        }
    }

    private void downloadFile(Uri inputUri, Uri outputUri) throws NullPointerException, IOException {
        LogUtils.LogDebug(TAG, "downloadFile");

        if (outputUri == null) {
            throw new NullPointerException("Output Uri is null - cannot download image");
        }

        OkHttpClient client = new OkHttpClient();

        BufferedSource source = null;
        Sink sink = null;
        Response response = null;
        try {
            Request request = new Request.Builder()
                    .url(inputUri.toString())
                    .build();
            response = client.newCall(request).execute();
            source = response.body().source();

            //安卓通过OutputUri获取OutputStream
            //OutputStream outputStream = mContext.getContentResolver().openOutputStream(outputUri);
            //鸿蒙通过OutputUri获取OutputStream
            OutputStream outputStream = null;
            DataAbilityHelper helper = DataAbilityHelper.creator(mContext);
            FileDescriptor fd = helper.openFile(outputUri, "w");
            outputStream = new FileOutputStream(fd);
            if (outputStream != null) {
                sink = Okio.sink(outputStream);
                source.readAll(sink);
            } else {
                throw new NullPointerException("OutputStream for given output Uri is null");
            }
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } finally {
            BitmapLoadUtils.close(source);
            BitmapLoadUtils.close(sink);
            if (response != null) {
                BitmapLoadUtils.close(response.body());
            }
            client.dispatcher().cancelAll();

            // swap uris, because input image was downloaded to the output destination
            // (cropped image will override it later)
            mInputUri = mOutputUri;
        }
    }


    private boolean checkSize(PixelMap bitmap, ImageSource.DecodingOptions options) {
        long bitmapSize = bitmap != null ? bitmap.getPixelBytesNumber() : 0;
        if (bitmapSize > MAX_BITMAP_SIZE) {
            options.sampleSize *= 2;
            return true;
        }
        return false;
    }
}
