/*
 *    Copyright 2017, Yalantis
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yalantis.ucrop.util;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.app.Context;
import ohos.media.image.PixelMap;

public class FastBitmapDrawable extends Component implements Component.DrawTask {

    private final Paint mPaint = new Paint();

    private PixelMap mBitmap;
    private int mAlpha;
    private int mWidth, mHeight;

    public FastBitmapDrawable(Context context, PixelMap b) {
        super(context);
        setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT));
        mAlpha = 255;
        setBitmap(b);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mBitmap != null && !mBitmap.isReleased()) {
            PixelMapHolder holder = new PixelMapHolder(mBitmap);
            LogUtils.LogInfo("qqqqqqqq:", "onDraw");
            canvas.drawPixelMapHolder(holder, 0, 0, new Paint());
        }
    }

    public void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
    }

    public int getOpacity() {
        return -3;
    }

    public void setFilterBitmap(boolean filterBitmap) {
        mPaint.setFilterBitmap(filterBitmap);
    }

    public float getAlpha() {
        return mAlpha;
    }

    public void setAlpha(int alpha) {
        mAlpha = alpha;
        mPaint.setAlpha(alpha);
    }

    public int getIntrinsicWidth() {
        return mWidth;
    }

    public int getIntrinsicHeight() {
        return mHeight;
    }

    public int getMinimumWidth() {
        return mWidth;
    }

    public int getMinimumHeight() {
        return mHeight;
    }

    public PixelMap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(PixelMap b) {
        addDrawTask(new DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {

            }
        });
        mBitmap = b;
        if (b != null) {
            mWidth = mBitmap.getImageInfo().size.width;
            mHeight = mBitmap.getImageInfo().size.height;
            LogUtils.LogInfo("qqqqqq:", "setBitmap:bitmap size:" + mHeight + " x " + mWidth);
        } else {
            mWidth = mHeight = 0;
        }
    }

}
