/*
 *    Copyright 2017, Yalantis
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yalantis.ucrop.util;

import ohos.agp.utils.Color;

public class Config {
    private Float targetAspectRatioX = new Float(0.0);
    private Float targetAspectRatioY = new Float(0.0);
    private Boolean mCircleDimmedLayer = false;
    private int mDimmedColor = 0x88000000; //透明黑

    private Boolean mShowCropFrame = true;


    private Boolean mShowCropGrid = true;

    int cropFrameStrokeSize = 2;
    int cropFrameColor = Color.RED.getValue();
    int cropGridStrokeSize = 2;
    int cropGridColor = Color.GRAY.getValue();
    int mCropGridRowCount = 2;
    int mCropGridColumnCount = 2;

    public int getmCropGridColumnCount() {
        return mCropGridColumnCount;
    }

    public int getmCropGridRowCount() {
        return mCropGridRowCount;
    }

    public void setmCropGridColumnCount(int mCropGridColumnCount) {
        this.mCropGridColumnCount = mCropGridColumnCount;
    }

    public void setmCropGridRowCount(int mCropGridRowCount) {
        this.mCropGridRowCount = mCropGridRowCount;
    }


    public int getCropFrameColor() {
        return cropFrameColor;
    }

    public int getCropFrameStrokeSize() {
        return cropFrameStrokeSize;
    }

    public int getCropGridColor() {
        return cropGridColor;
    }

    public int getCropGridStrokeSize() {
        return cropGridStrokeSize;
    }

    public void setCropFrameColor(int cropFrameColor) {
        this.cropFrameColor = cropFrameColor;
    }

    public void setCropFrameStrokeSize(int cropFrameStrokeSize) {
        this.cropFrameStrokeSize = cropFrameStrokeSize;
    }

    public void setCropGridColor(int cropGridColor) {
        this.cropGridColor = cropGridColor;
    }

    public void setCropGridStrokeSize(int cropGridStrokeSize) {
        this.cropGridStrokeSize = cropGridStrokeSize;
    }


    public Boolean getmShowCropFrame() {
        return mShowCropFrame;
    }

    public Boolean getmShowCropGrid() {
        return mShowCropGrid;
    }

    public void setmShowCropFrame(Boolean mShowCropFrame) {
        this.mShowCropFrame = mShowCropFrame;
    }

    public void setmShowCropGrid(Boolean mShowCropGrid) {
        this.mShowCropGrid = mShowCropGrid;
    }

    public void setmCircleDimmedLayer(Boolean mCircleDimmedLayer) {
        this.mCircleDimmedLayer = mCircleDimmedLayer;
    }

    public int getmDimmedColor() {
        return mDimmedColor;
    }

    public Boolean getmCircleDimmedLayer() {
        return mCircleDimmedLayer;
    }

    public void setmDimmedColor(int mDimmedColor) {
        this.mDimmedColor = mDimmedColor;
    }

    public void setTargetAspectRatioX(Float targetAspectRatioX) {
        this.targetAspectRatioX = targetAspectRatioX;
    }

    public void setTargetAspectRatioY(Float targetAspectRatioY) {
        this.targetAspectRatioY = targetAspectRatioY;
    }

    public Float getTargetAspectRatioX() {
        return targetAspectRatioX;
    }

    public Float getTargetAspectRatioY() {
        return targetAspectRatioY;
    }
}
