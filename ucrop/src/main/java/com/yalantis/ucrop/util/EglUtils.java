/*
 *    Copyright 2017, Yalantis
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yalantis.ucrop.util;

import ohos.agp.render.opengl.*;


/**
 * Created by Oleksii Shliama [https://github.com/shliama] on 9/8/16.
 */
public class EglUtils {

    private static final String TAG = "EglUtils";

    private EglUtils() {

    }

    public static int getMaxTextureSize() {
        try {

                return getMaxTextureEgl14();

        } catch (Exception e) {
            LogUtils.LogError(TAG, "getMaxTextureSize: "+ e);
            return 0;
        }
    }

    private static int getMaxTextureEgl14() {
        EGLDisplay dpy = EGL.eglGetDisplay(EGL.EGL_DEFAULT_DISPLAY);
        int[] vers = new int[2];
        EGL.eglInitialize(dpy,  vers,  vers);//【风险】

        int[] configAttr = {
                EGL.EGL_COLOR_BUFFER_TYPE, EGL.EGL_RGB_BUFFER,
                EGL.EGL_LEVEL, 0,
                EGL.EGL_RENDERABLE_TYPE,
                EGL.EGL_SURFACE_TYPE, EGL.EGL_PBUFFER_BIT,
                EGL.EGL_NONE
        };
        //原来还有个参数：EGL.EGL_OPENGL_ES2_BIT
        EGLConfig[] configs = new EGLConfig[1];
        int[] numConfig = new int[1];
        EGL.eglChooseConfig(dpy, configAttr,
                configs, 1, numConfig);
        if (numConfig[0] == 0) {
            return 0;
        }
        EGLConfig config = configs[0];

        int[] surfAttr = {
                EGL.EGL_WIDTH, 64,
                EGL.EGL_HEIGHT, 64,
                EGL.EGL_NONE
        };
        EGLSurface surf = EGL.eglCreatePbufferSurface(dpy, config, surfAttr);

        int[] ctxAttrib = {
                2,
                EGL.EGL_NONE
        };
        EGLContext ctx = EGL.eglCreateContext(dpy, config, EGL.EGL_NO_CONTEXT, ctxAttrib);

        EGL.eglMakeCurrent(dpy, surf, surf, ctx);

        int[] maxSize = new int[1];
        GLES20.glGetIntegerv(GLES20.GL_MAX_TEXTURE_SIZE, maxSize);

        EGL.eglMakeCurrent(dpy, EGL.EGL_NO_SURFACE, EGL.EGL_NO_SURFACE,
                EGL.EGL_NO_CONTEXT);
        EGL.eglDestroySurface(dpy, surf);
        EGL.eglDestroyContext(dpy, ctx);
        EGL.eglTerminate(dpy);

        return maxSize[0];
    }
}
