/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing_impl.adapter;

import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing_impl.ResourceTable;
import com.bilibili.boxing_impl.WindowManagerHelper;
import com.bilibili.boxing_impl.view.MediaItemLayout;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;


/**
 * A RecyclerView.Adapter for image or video picker showing.
 */
public class BoxingMediaAdapter extends BaseItemProvider {

    private static final int CAMERA_TYPE = 0;
    private static final int NORMAL_TYPE = 1;

    private int mOffset;
    private boolean mMultiImageMode;

    private List<BaseMedia> mMedias;
    private List<BaseMedia> mSelectedMedias;
    private BoxingConfig mMediaConfig;
    private Component.ClickedListener mOnCameraClickListener;
    private Component.ClickedListener mOnMediaClickListener;
    private OnMediaCheckedListener mOnCheckedListener;
    private int mDefaultRes;

    private Ability mAbility;
    private final int mColumnNum = 3;
    private final int mItemMargin = 6;
    private final int mItemMarginWidth = 20;


    public BoxingMediaAdapter(Ability context) {
        this.mAbility = context;
        this.mMedias = new ArrayList<>();
        this.mSelectedMedias = new ArrayList<>(9);
        this.mMediaConfig = BoxingManager.getInstance().getBoxingConfig();
        this.mOffset = mMediaConfig.isNeedCamera() ? 1 : 0;
        this.mMultiImageMode = mMediaConfig.getMode() == BoxingConfig.Mode.MULTI_IMG;
        this.mDefaultRes = mMediaConfig.getMediaPlaceHolderRes();
    }

    @Override
    public int getItemComponentType(int position) {
        if (position == 0 && mMediaConfig.isNeedCamera()) {
            return CAMERA_TYPE;
        }
        return NORMAL_TYPE;
    }

    @Override
    public int getComponentTypeCount() {
        return 2;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {

        ImageViewHolder imageViewHolder = null;
        if (component == null) {
            if (getItemComponentType(position) == CAMERA_TYPE) {
                component = LayoutScatter.getInstance(mAbility).parse(ResourceTable.Layout_layout_boxing_recycleview_header, null, false);
                Component camera_layout = component.findComponentById(ResourceTable.Id_camera_layout);
                int screenWidth = WindowManagerHelper.getScreenWidth(mAbility.getContext());
                ComponentContainer.LayoutConfig layoutConfig = camera_layout.getLayoutConfig();
                layoutConfig.width = screenWidth / mColumnNum - mItemMarginWidth;
                layoutConfig.height = screenWidth / mColumnNum - mItemMarginWidth;
                layoutConfig.setMargins(mItemMargin, mItemMargin, mItemMargin, mItemMargin);
                camera_layout.setLayoutConfig(layoutConfig);
                camera_layout.setClickedListener(mOnCameraClickListener);
            } else {
                component = LayoutScatter.getInstance(mAbility).parse(ResourceTable.Layout_layout_boxing_recycleview_item, null, false);
                imageViewHolder = new ImageViewHolder();
                imageViewHolder.parentV = (DirectionalLayout) component.findComponentById(ResourceTable.Id_parentV);
                imageViewHolder.mItemLayout = (MediaItemLayout) component.findComponentById(ResourceTable.Id_media_layout);
                imageViewHolder.mItemChecked = component.findComponentById(ResourceTable.Id_media_item_check);
                component.setTag(imageViewHolder);
            }
        } else {
            imageViewHolder = (ImageViewHolder) component.getTag();
        }

        if (getItemComponentType(position) == NORMAL_TYPE) {
            int pos = position - mOffset;
            final BaseMedia media = mMedias.get(pos);
            imageViewHolder.mItemLayout.setImageRes(mDefaultRes);
            imageViewHolder.mItemLayout.setTag(media);
            imageViewHolder.mItemLayout.setClickedListener(mOnMediaClickListener);
            imageViewHolder.mItemLayout.setMedia(mAbility, media);
            imageViewHolder.mItemChecked.setVisibility(mMultiImageMode ? Component.VISIBLE : Component.HIDE);
            if (mMultiImageMode && media instanceof ImageMedia) {
                imageViewHolder.mItemChecked.setTag(imageViewHolder.mItemLayout);
                imageViewHolder.mItemChecked.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        MediaItemLayout itemLayout = (MediaItemLayout) component.getTag();
                        if (mMediaConfig.getMode() == BoxingConfig.Mode.MULTI_IMG) {
                            if (mOnCheckedListener != null) {
                                mOnCheckedListener.onChecked(itemLayout, (BaseMedia) getItem(pos));
                            }
                        }
                    }
                });
            }

            int screenWidth = WindowManagerHelper.getScreenWidth(mAbility.getContext());
            ComponentContainer.LayoutConfig layoutConfig = imageViewHolder.mItemLayout.getLayoutConfig();
            layoutConfig.width = screenWidth / mColumnNum - mItemMarginWidth;
            layoutConfig.height = screenWidth / mColumnNum - mItemMarginWidth;
            layoutConfig.setMargins(mItemMargin, mItemMargin, mItemMargin, mItemMargin);
            imageViewHolder.mItemLayout.setLayoutConfig(layoutConfig);
        }
        return component;
    }

    @Override
    public Object getItem(int i) {
        return mMedias.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mMedias.size() + mOffset;
    }

    private class ImageViewHolder {
        DirectionalLayout parentV;
        MediaItemLayout mItemLayout;
        Component mItemChecked;
    }

    public void setOnCameraClickListener(Component.ClickedListener onCameraClickListener) {
        mOnCameraClickListener = onCameraClickListener;
    }

    public void setOnCheckedListener(OnMediaCheckedListener onCheckedListener) {
        mOnCheckedListener = onCheckedListener;
    }

    public void setOnMediaClickListener(Component.ClickedListener onMediaClickListener) {
        mOnMediaClickListener = onMediaClickListener;
    }

    public List<BaseMedia> getSelectedMedias() {
        return mSelectedMedias;
    }

    public void setSelectedMedias(List<BaseMedia> selectedMedias) {
        if (selectedMedias == null) {
            return;
        }
        mSelectedMedias.clear();
        mSelectedMedias.addAll(selectedMedias);
        notifyDataChanged();
    }

    public void addAllData(List<BaseMedia> data) {
        int oldSize = mMedias.size();
        this.mMedias.addAll(data);
        int size = data.size();
        notifyDataSetItemRangeInserted(oldSize, size);
    }

    public void clearData() {
        int size = mMedias.size();
        this.mMedias.clear();
        notifyDataSetItemRangeRemoved(0, size);
    }

    public List<BaseMedia> getAllMedias() {
        return mMedias;
    }

    public interface OnMediaCheckedListener {
        /**
         * In multi image mode, selecting a {@link BaseMedia} or undo.
         */
        void onChecked(Component v, BaseMedia iMedia);
    }

}
