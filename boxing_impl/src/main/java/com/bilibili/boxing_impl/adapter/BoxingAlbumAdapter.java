/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing_impl.adapter;

import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.entity.AlbumEntity;
import com.bilibili.boxing_impl.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Album window adapter.
 */
public class BoxingAlbumAdapter extends BaseItemProvider {

    private int mCurrentAlbumPos;
    private List<AlbumEntity> mAlums;
    private OnAlbumClickListener mAlbumOnClickListener;
    private int mDefaultRes;
    private Context mContext;
    private Ability mAbility;
    private static final String UNKNOW_ALBUM_NAME = "?";

    public BoxingAlbumAdapter(Ability ability, Context context) {
        this.mAbility = ability;
        this.mContext = context;
        this.mAlums = new ArrayList<>();
        this.mAlums.add(AlbumEntity.createDefaultAlbum());
        this.mDefaultRes = BoxingManager.getInstance().getBoxingConfig().getAlbumPlaceHolderRes();
    }

    public void setAlbumOnClickListener(OnAlbumClickListener albumOnClickListener) {
        this.mAlbumOnClickListener = albumOnClickListener;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {

        AlbumViewHolder viewHolder = null;
        if (component == null) {
            component = LayoutScatter.getInstance(mAbility).parse(ResourceTable.Layout_layout_boxing_album_item, null, false);
            viewHolder = new AlbumViewHolder();

            viewHolder.mLayout = component.findComponentById(ResourceTable.Id_album_layout);
            viewHolder.mCoverImg = (Image) component.findComponentById(ResourceTable.Id_album_thumbnail);
            viewHolder.mNameTxt = (Text) component.findComponentById(ResourceTable.Id_album_name);
            viewHolder.mSizeTxt = (Text) component.findComponentById(ResourceTable.Id_album_size);
            viewHolder.mCheckedImg = (Image) component.findComponentById(ResourceTable.Id_album_checked);
            component.setTag(viewHolder);
        } else {
            viewHolder = (AlbumViewHolder) component.getTag();
        }

        final AlbumEntity album = mAlums.get(i);
        viewHolder.mCoverImg.setPixelMap(mDefaultRes);
        if (album != null && album.hasImages()) {
            String albumName = album.mBucketName.isEmpty() ? "所有相片" : album.mBucketName;
            viewHolder.mNameTxt.setText(albumName);

            viewHolder.mCheckedImg.setVisibility(album.mIsSelected ? Component.VISIBLE : Component.HIDE);
            viewHolder.mSizeTxt.setText("（" + album.mCount + "）");
            viewHolder.mLayout.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (mAlbumOnClickListener != null) {
                        mAlbumOnClickListener.onClick(component, i);
                    }
                }
            });
        } else {
            viewHolder.mNameTxt.setText(UNKNOW_ALBUM_NAME);
            viewHolder.mSizeTxt.setVisibility(Component.HIDE);
        }
        return component;
    }

    public void addAllData(List<AlbumEntity> alums) {
        mAlums.clear();
        mAlums.addAll(alums);
        notifyDataChanged();
    }

    public List<AlbumEntity> getAlums() {
        return mAlums;
    }

    public int getCurrentAlbumPos() {
        return mCurrentAlbumPos;
    }

    public void setCurrentAlbumPos(int currentAlbumPos) {
        mCurrentAlbumPos = currentAlbumPos;
    }

    public AlbumEntity getCurrentAlbum() {
        if (mAlums == null || mAlums.size() <= 0) {
            return null;
        }
        return mAlums.get(mCurrentAlbumPos);
    }

    @Override
    public int getCount() {
        return mAlums != null ? mAlums.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return mAlums.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private static class AlbumViewHolder {
        Image mCoverImg;
        Text mNameTxt;
        Text mSizeTxt;
        Component mLayout;
        Image mCheckedImg;
    }

    public interface OnAlbumClickListener {
        void onClick(Component view, int pos);
    }
}
