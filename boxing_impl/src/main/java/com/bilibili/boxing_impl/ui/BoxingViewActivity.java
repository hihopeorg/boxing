/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing_impl.ui;

import com.bilibili.boxing.AbsBoxingViewActivity;
import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.ResultMedia;
import com.bilibili.boxing.model.task.IMediaTask;
import com.bilibili.boxing_impl.ResourceTable;
import com.bilibili.boxing_impl.view.HackyViewPager;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * An Activity to show raw image by holding {@link BoxingViewFragment}.
 */
public class BoxingViewActivity extends AbsBoxingViewActivity {
    public static final String EXTRA_TYPE_BACK = "com.bilibili.boxing_impl.ui.BoxingViewActivity.type_back";

    HackyViewPager mPageSlider;
    private boolean mNeedEdit;
    private boolean mNeedLoading;
    private boolean mFinishLoading;
    private boolean mNeedAllCount = true;
    private int mCurrentPage;
    private int mTotalCount;
    private int mStartPos;
    private int mPos;
    private int mMaxCount;

    private String mAlbumId;
    private Text mTitleText;
    private Button mOkBtn;
    private ArrayList<BaseMedia> mImages;
    private ArrayList<BaseMedia> mSelectedImages;
    private ArrayList<ResultMedia> mSelectedImagesResult;
    private List<Component> mPages;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_boxing_view);
        initData();
        initView();
        startLoading();
    }

    private void initData() {
        mSelectedImages = getSelectedImages();
        mSelectedImagesResult = getSelectedImagesResult();
        mAlbumId = getAlbumId();
        mStartPos = getStartPos();
        mNeedLoading = BoxingManager.getInstance().getBoxingConfig().isNeedLoading();
        mNeedEdit = BoxingManager.getInstance().getBoxingConfig().isNeedEdit();
        mMaxCount = getMaxCount();
        mImages = new ArrayList<>();
        if (!mNeedLoading && mSelectedImages != null) {
            mImages.addAll(mSelectedImages);
        }
    }

    private void initView() {
        mTitleText = (Text) findComponentById(ResourceTable.Id_titleText);
        mOkBtn = (Button) findComponentById(ResourceTable.Id_image_items_ok);
        mPageSlider = (HackyViewPager) findComponentById(ResourceTable.Id_pager);
        findComponentById(ResourceTable.Id_backImage).setClickedListener(component -> onBackPressed());
        if (!mNeedEdit) {
            Component chooseLayout = findComponentById(ResourceTable.Id_item_choose_layout);
            chooseLayout.setVisibility(Component.HIDE);
        } else {
            setOkTextNumber();
            mOkBtn.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    finishByBackPressed(false);
                }
            });
        }
    }

    private void setOkTextNumber() {
        if (mNeedEdit) {
            int selectedSize = mSelectedImages.size();
            int size = Math.max(mSelectedImages.size(), mMaxCount);
            mOkBtn.setText(selectedSize + "/" + size + "确定");
            mOkBtn.setEnabled(selectedSize > 0);
        }
    }

    private void finishByBackPressed(boolean value) {

        String[] str = new String[mSelectedImagesResult.size()];
        for (int i = 0; i < mSelectedImagesResult.size(); i++) {
            str[i] = mSelectedImagesResult.get(i).getId();
        }

        Intent intent = new Intent();
        intent.setParam(EXTRA_TYPE_BACK, value);
        intent.setParam(Boxing.EXTRA_RESULT_ID, str);
        setResult(Boxing.COMPRESS_RESULT_CODE, intent);
        terminateAbility();
    }

    @Override
    public void startLoading() {
        if (!mNeedLoading) {
            mTitleText.setText((mStartPos + 1) + "/" + mSelectedImages.size());
            mPageSlider.setVisibility(Component.VISIBLE);
            initPageSlider(mSelectedImagesResult);
            if (mStartPos > 0 && mStartPos < mSelectedImages.size()) {
                mPageSlider.setCurrentPage(mStartPos, false);
            }
        } else {
            loadMedia(mAlbumId, mStartPos, mCurrentPage);
            initPageSlider(mSelectedImagesResult);
        }
    }

    private void initPageSlider(ArrayList<ResultMedia> images) {

        mPages = new ArrayList<>();
        LayoutScatter scatter = LayoutScatter.getInstance(getContext());
        for (int i = 0; i < images.size(); i++) {
            DirectionalLayout page0 = (DirectionalLayout) scatter.parse(ResourceTable.Layout_layout_boxing_raw_image, null, false);
            mPages.add(page0);
            Image image = (Image) page0.findComponentById(ResourceTable.Id_image);

            DataAbilityHelper helper = DataAbilityHelper.creator(this);
            ResultMedia imageMedia = images.get(i);
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(imageMedia.getId()));
            try {
                FileDescriptor fd = helper.openFile(uri, "r");
                ImageSource imageSource = ImageSource.create(fd, null);
                PixelMap pixelMap = imageSource.createPixelmap(null);
                image.setPixelMap(pixelMap);
            } catch (DataAbilityRemoteException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        mPageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(mPages.get(i));
                return mPages.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(mPages.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        mPageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                mTitleText.setText((i + 1) + "/" + mSelectedImages.size());
            }
        });
    }

    private void loadMedia(String albumId, int startPos, int page) {
        this.mPos = startPos;
        loadMedias(page, albumId);
    }

    @Override
    public void showMedia(List<BaseMedia> medias, int totalCount) {
        if (medias == null || totalCount <= 0) {
            return;
        }
        mImages.addAll(medias);
        checkSelectedMedia(mImages, mSelectedImages);
        setupGallery();

        if (mNeedAllCount) {
            mNeedAllCount = false;
        }
        loadOtherPagesInAlbum(totalCount);
    }

    private void setupGallery() {
        int startPos = mStartPos;
        if (mPageSlider == null || startPos < 0) {
            return;
        }
        if (startPos < mImages.size() && !mFinishLoading) {
            mPageSlider.setCurrentPage(mStartPos, false);
            mPageSlider.setVisibility(Component.VISIBLE);
            mFinishLoading = true;
        } else if (startPos >= mImages.size()) {
            mPageSlider.setVisibility(Component.HIDE);
        }
    }

    private void loadOtherPagesInAlbum(int totalCount) {
        mTotalCount = totalCount;
        if (mCurrentPage <= (mTotalCount / IMediaTask.PAGE_LIMIT)) {
            mCurrentPage++;
            loadMedia(mAlbumId, mStartPos, mCurrentPage);
        }
    }

    @Override
    protected void onActive() {

        if (mSelectedImages != null) {
            getIntent().setParam(Boxing.EXTRA_SELECTED_MEDIA, mSelectedImages);
        }
        getIntent().setParam(Boxing.EXTRA_ALBUM_ID, mAlbumId);
        super.onActive();
    }

    @Override
    public void onBackPressed() {
        finishByBackPressed(true);
    }
}
