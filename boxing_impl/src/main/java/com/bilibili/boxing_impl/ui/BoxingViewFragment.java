/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing_impl.ui;

import com.bilibili.boxing.AbsBoxingViewFragment;
import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.CropPictureAbility;
import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.AlbumEntity;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing.utils.BoxingFileHelper;
import com.bilibili.boxing_impl.ResourceTable;
import com.bilibili.boxing_impl.adapter.BoxingAlbumAdapter;
import com.bilibili.boxing_impl.adapter.BoxingMediaAdapter;
import com.bilibili.boxing_impl.view.MediaItemLayout;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * A full implement for {@link com.bilibili.boxing.presenter.PickerContract.View} supporting all the mode
 * in {@link BoxingConfig.Mode}.
 * use this to pick the picture.
 */
public class BoxingViewFragment extends AbsBoxingViewFragment implements Component.ClickedListener {

    private static final int IMAGE_PREVIEW_REQUEST_CODE = 9086;
    private static final int IMAGE_CROP_REQUEST_CODE = 9087;

    private static final int GRID_COUNT = 3;

    public static boolean mIsPreview;
    private boolean mIsCamera;

    private Button mPreBtn;
    private Button mOkBtn;
    private ListContainer mRecycleView;
    private Component mMultiImageLayout;
    private BoxingMediaAdapter mMediaAdapter;
    private BoxingAlbumAdapter mAlbumWindowAdapter;
    private Text mEmptyTxt;
    private Text mTitleTxt;
    private PopupDialog mAlbumPopWindow;
    private int mMaxCount;
    private Component mComponent;

    public static BoxingViewFragment newInstance() {
        return new BoxingViewFragment();
    }

    @Override
    public void onCreateWithSelectedMedias(Intent savedInstanceState, List<BaseMedia> selectedMedias) {
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mComponent = scatter.parse(ResourceTable.Layout_fraction_boxing_view, container, false);
        return mComponent;
    }

    @Override
    protected void onStart(Intent intent) {
        initViews(mComponent);
        super.onStart(intent);
    }

    private void initViews(Component view) {
        mEmptyTxt = (Text) view.findComponentById(ResourceTable.Id_empty_txt);
        mRecycleView = (ListContainer) view.findComponentById(ResourceTable.Id_listContainer);
        initRecycleView();

        boolean isMultiImageMode = BoxingManager.getInstance().getBoxingConfig().isMultiImageMode();
        mMultiImageLayout = view.findComponentById(ResourceTable.Id_multi_picker_layout);
        mMultiImageLayout.setVisibility(isMultiImageMode ? Component.VISIBLE : Component.HIDE);
        if (isMultiImageMode) {
            mPreBtn = (Button) view.findComponentById(ResourceTable.Id_choose_preview_btn);
            mOkBtn = (Button) view.findComponentById(ResourceTable.Id_choose_ok_btn);

            mPreBtn.setClickedListener(this::onClick);
            mOkBtn.setClickedListener(this::onClick);
            updateMultiPickerLayoutState(mMediaAdapter.getSelectedMedias());
        }
    }

    private void initRecycleView() {
        mAlbumWindowAdapter = new BoxingAlbumAdapter(getFractionAbility(), getContext());
        mMediaAdapter = new BoxingMediaAdapter(getFractionAbility());
        mMaxCount = getMaxCount();

        TableLayoutManager manager = new TableLayoutManager();
        manager.setColumnCount(GRID_COUNT);
        mRecycleView.setLayoutManager(manager);

        mMediaAdapter.setOnCameraClickListener(new OnCameraClickListener());
        mMediaAdapter.setOnCheckedListener(new OnMediaCheckedListener());
        mMediaAdapter.setOnMediaClickListener(new OnMediaClickListener());
        mRecycleView.setItemProvider(mMediaAdapter);
        mRecycleView.setScrolledListener(new ScrollListener());
    }

    @Override
    public void startLoading() {
        loadMedias();
        loadAlbum();
    }

    @Override
    public void onRequestPermissionError(String[] permissions, Exception e) {
        if (permissions.length > 0) {
            if (permissions[0].equals("ohos.permission.WRITE_MEDIA")) {
                new ToastDialog(getFractionAbility()).setText(getString(ResourceTable.String_boxing_storage_permission_deny)).show();
                showEmptyData();
            } else if (permissions[0].equals("ohos.permission.CAMERA")) {
                new ToastDialog(getFractionAbility()).setText(getString(ResourceTable.String_boxing_camera_permission_deny)).show();
            }
        }
    }

    @Override
    public void onRequestPermissionSuc(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions[0].equals("ohos.permission.WRITE_MEDIA")) {
            startLoading();
        } else if (permissions[0].equals("ohos.permission.CAMERA")) {
            startCamera(getFractionAbility(), this, null);
        }
    }

    @Override
    public void showMedia(List<BaseMedia> medias, int allCount) {
        if (medias == null || isEmptyData(medias) && isEmptyData(mMediaAdapter.getAllMedias())) {
            showEmptyData();
            return;
        }
        showData();
        mMediaAdapter.addAllData(medias);
    }

    private boolean isEmptyData(List<BaseMedia> medias) {
        return medias.isEmpty() && !BoxingManager.getInstance().getBoxingConfig().isNeedCamera();
    }

    private void showEmptyData() {
        mEmptyTxt.setVisibility(Component.VISIBLE);
        mRecycleView.setVisibility(Component.HIDE);
    }

    private void showData() {
        mEmptyTxt.setVisibility(Component.HIDE);
        mRecycleView.setVisibility(Component.VISIBLE);
    }

    /**
     * @param albums the results of albums
     */
    @Override
    public void showAlbum(List<AlbumEntity> albums) {
        if ((albums == null || albums.isEmpty()) && mTitleTxt != null) {
            mTitleTxt.setAroundElements(null, null, null, null);
            mTitleTxt.setClickedListener(null);
            return;
        }
        mAlbumWindowAdapter.addAllData(albums);
    }

    public BoxingMediaAdapter getMediaAdapter() {
        return mMediaAdapter;
    }

    public BoxingAlbumAdapter getAlbumAdapter() {
        return mAlbumWindowAdapter;
    }

    @Override
    public void clearMedia() {
        mMediaAdapter.clearData();
    }

    private void updateMultiPickerLayoutState(List<BaseMedia> medias) {
        updateOkBtnState(medias);
        updatePreviewBtnState(medias);
    }

    private void updatePreviewBtnState(List<BaseMedia> medias) {
        if (mPreBtn == null || medias == null) {
            return;
        }
        boolean enabled = medias.size() > 0 && medias.size() <= mMaxCount;
        mPreBtn.setEnabled(enabled);
    }

    private void updateOkBtnState(List<BaseMedia> medias) {
        if (mOkBtn == null || medias == null) {
            return;
        }
        boolean enabled = medias.size() > 0 && medias.size() <= mMaxCount;
        mOkBtn.setEnabled(enabled);
        mOkBtn.setText(enabled ? "(" + medias.size() + "/" + mMaxCount + ") 确定" : "确定");
    }

    @Override
    public void onCameraFinish(BaseMedia media) {
        mIsCamera = false;
        if (media == null) {
            return;
        }
        if (hasCropBehavior()) {
            startCrop(media, IMAGE_CROP_REQUEST_CODE);
        } else if (mMediaAdapter != null && mMediaAdapter.getSelectedMedias() != null) {
            List<BaseMedia> selectedMedias = mMediaAdapter.getSelectedMedias();
            selectedMedias.add(media);
            onFinish(selectedMedias);
        }
    }

    @Override
    public void onCameraError() {
        mIsCamera = false;
    }

    @Override
    public void onClick(Component v) {
        int id = v.getId();
        if (id == ResourceTable.Id_choose_ok_btn) {
            onFinish(mMediaAdapter.getSelectedMedias());
        } else if (id == ResourceTable.Id_choose_preview_btn) {
            if (!mIsPreview) {
                mIsPreview = true;
                ArrayList<BaseMedia> medias = (ArrayList<BaseMedia>) mMediaAdapter.getSelectedMedias();
                Boxing.get().withIntent(getFractionAbility(), BoxingViewActivity.class, medias)
                        .start(this, BoxingViewFragment.IMAGE_PREVIEW_REQUEST_CODE, BoxingConfig.ViewMode.PRE_EDIT, mBundleName, BoxingViewActivity.class.getName());
            }
        }
    }

    @Override
    public void onCameraActivityResult(int requestCode, int resultCode) {
        super.onCameraActivityResult(requestCode, resultCode);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        ArrayList<BaseMedia> medias = (ArrayList<BaseMedia>) getMediaAdapter().getSelectedMedias();
        onSaveMedias(getFractionAbility().getIntent(), medias);
    }

    public void setTitleTxt(Text titleTxt) {
        mTitleTxt = titleTxt;
        mTitleTxt.setClickedListener(component -> {
            Component layout = LayoutScatter.getInstance(getFractionAbility().getContext())
                    .parse(ResourceTable.Layout_layout_boxing_album, null, false);
            mAlbumPopWindow = new PopupDialog(getFractionAbility().getContext(), mRecycleView);
            mAlbumPopWindow.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
            mAlbumPopWindow.setTransparent(true);
            mAlbumPopWindow.setCustomComponent(layout);
            mAlbumPopWindow.setAutoClosable(true);
            mAlbumPopWindow.show();
            backgroundAlpha(0.5f);
            mAlbumPopWindow.setDialogListener(new BaseDialog.DialogListener() {
                @Override
                public boolean isTouchOutside() {
                    backgroundAlpha(1f);
                    return false;
                }
            });
            mAlbumWindowAdapter.setAlbumOnClickListener(new OnAlbumItemOnClickListener());
            ListContainer listContainer = (ListContainer) layout.findComponentById(ResourceTable.Id_listContainer);
            listContainer.setItemProvider(mAlbumWindowAdapter);
            mAlbumWindowAdapter.notifyDataChanged();
        });
    }

    private void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutConfig layoutConfig = getFractionAbility().getWindow().getLayoutConfig().get();
        layoutConfig.alpha = bgAlpha;
        getFractionAbility().getWindow().setLayoutConfig(layoutConfig);
    }

    private void dismissAlbumWindow() {
        if (mAlbumPopWindow != null && mAlbumPopWindow.isShowing()) {
            mAlbumPopWindow.hide();
        }
    }

    private class ScrollListener implements ListContainer.ScrolledListener {

        @Override
        public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
            final int childCount = ((ListContainer) component).getChildCount();
            if (childCount > 0) {
                Component lastChild = ((ListContainer) component).getComponentAt(childCount - 1);
                BoxingMediaAdapter outerAdapter = (BoxingMediaAdapter) ((ListContainer) component).getItemProvider();
                int lastVisible = ((ListContainer) component).getIndexForComponent(lastChild);
                if (lastVisible == outerAdapter.getCount() - 1 && hasNextPage() && canLoadNextPage()) {
                    onLoadNextPage();
                }
            }
        }
    }

    private class OnMediaClickListener implements Component.ClickedListener {

        @Override
        public void onClick(Component v) {
            BaseMedia media = (BaseMedia) v.getTag();
            media.getId();
            BoxingConfig.Mode mode = BoxingManager.getInstance().getBoxingConfig().getMode();
            if (mode == BoxingConfig.Mode.SINGLE_IMG) {
                singleImageClick(media);
            } else if (mode == BoxingConfig.Mode.MULTI_IMG) {
                multiImageClick(media, 0);
            } else if (mode == BoxingConfig.Mode.VIDEO) {
                videoClick(media);
            }
        }

        private void videoClick(BaseMedia media) {
            ArrayList<BaseMedia> iMedias = new ArrayList<>();
            iMedias.add(media);
            onFinish(iMedias);
        }

        private void multiImageClick(BaseMedia baseMedia, int pos) {
            if (!mIsPreview) {
                mIsPreview = true;
                ArrayList<BaseMedia> medias = new ArrayList<>();
                ImageMedia photoMedia = (ImageMedia) baseMedia;
                medias.add(photoMedia);
                Boxing.get().withIntent(BoxingViewFragment.this, BoxingViewActivity.class, medias)
                        .start(BoxingViewFragment.this, BoxingViewFragment.IMAGE_PREVIEW_REQUEST_CODE, BoxingConfig.ViewMode.PRE_EDIT, mBundleName, BoxingViewActivity.class.getName());
            }
        }

        private void singleImageClick(BaseMedia media) {
            ArrayList<BaseMedia> iMedias = new ArrayList<>();
            iMedias.add(media);
            if (hasCropBehavior()) { //Cutting
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, media.getId());
                Intent intent = new Intent();
                intent.setUri(uri);
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(mBundleName)
                        .withAbilityName(CropPictureAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                getFractionAbility().startAbilityForResult(intent, 1001);
            } else {
                onFinish(iMedias);
            }
        }
    }

    private class OnCameraClickListener implements Component.ClickedListener {

        @Override
        public void onClick(Component v) {
            if (!mIsCamera) {
                mIsCamera = true;
                startCamera(getFractionAbility(), BoxingViewFragment.this, BoxingFileHelper.DEFAULT_SUB_DIR);
            }
        }
    }

    private class OnMediaCheckedListener implements BoxingMediaAdapter.OnMediaCheckedListener {

        @Override
        public void onChecked(Component view, BaseMedia iMedia) {
            if (!(iMedia instanceof ImageMedia)) {
                return;
            }
            ImageMedia photoMedia = (ImageMedia) iMedia;
            boolean isSelected = !photoMedia.isSelected();
            MediaItemLayout layout = (MediaItemLayout) view;
            List<BaseMedia> selectedMedias = mMediaAdapter.getSelectedMedias();
            if (isSelected) {
                if (selectedMedias.size() >= mMaxCount) {
                    String warning = "您最多只能选择" + mMaxCount + "张图片";
                    new ToastDialog(getFractionAbility()).setText(warning).show();
                    return;
                }
                if (!selectedMedias.contains(photoMedia)) {
                    if (photoMedia.isGifOverSize()) {
                        new ToastDialog(getFractionAbility()).setText("您选择的gif图片过大，请压缩后再上传").show();
                        return;
                    }
                    selectedMedias.add(photoMedia);
                }
            } else {
                if (selectedMedias.size() >= 1 && selectedMedias.contains(photoMedia)) {
                    selectedMedias.remove(photoMedia);
                }
            }
            photoMedia.setSelected(isSelected);
            layout.setChecked(isSelected);
            updateMultiPickerLayoutState(selectedMedias);
        }
    }

    private class OnAlbumItemOnClickListener implements BoxingAlbumAdapter.OnAlbumClickListener {

        @Override
        public void onClick(Component view, int pos) {
            backgroundAlpha(1f);
            BoxingAlbumAdapter adapter = mAlbumWindowAdapter;
            if (adapter != null && adapter.getCurrentAlbumPos() != pos) {
                List<AlbumEntity> albums = adapter.getAlums();
                adapter.setCurrentAlbumPos(pos);

                AlbumEntity albumMedia = albums.get(pos);
                loadMedias(0, albumMedia.mBucketId);
                if (albumMedia.mBucketName == null || albumMedia.mBucketName.isEmpty()) {
                    mTitleTxt.setText("所有相片");
                } else {
                    mTitleTxt.setText(albumMedia.mBucketName);
                }
                for (AlbumEntity album : albums) {
                    album.mIsSelected = false;
                }
                albumMedia.mIsSelected = true;
                adapter.notifyDataChanged();

                mMediaAdapter.getAllMedias().clear();
                mMediaAdapter.addAllData(albumMedia.mImageList);
                mMediaAdapter.notifyDataChanged();
            }
            dismissAlbumWindow();
        }
    }

}
