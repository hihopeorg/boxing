/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing_impl.ui;

import com.bilibili.boxing.AbsBoxingActivity;
import com.bilibili.boxing.AbsBoxingViewFragment;
import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing_impl.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.utils.PlainArray;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.bilibili.boxing_impl.ui.BoxingViewActivity.EXTRA_TYPE_BACK;

/**
 * Default UI Activity for simplest usage.
 * A simple subclass of {@link AbsBoxingActivity}. Holding a {@link AbsBoxingViewFragment} to display medias.
 */
public class BoxingActivity extends AbsBoxingActivity {

    private FractionManager mFractionManager;
    private Fraction mCurFraction;
    private List<Fraction> mFractions = new ArrayList<>();
    private PlainArray<Fraction> mFractionPlainArray = new PlainArray<>();
    private BoxingViewFragment mPickerFragment;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_boxing);
        setTitleTxt(getBoxingConfig());
        mFractions.add(mPickerFragment);
        mFractionManager = getFractionManager();
        createPageInContainer(ResourceTable.Id_stackLayout, 0);
    }

    //    @NonNull
    @Override
    public AbsBoxingViewFragment onCreateBoxingView(ArrayList<BaseMedia> medias) {
        mPickerFragment = (BoxingViewFragment) BoxingViewFragment.newInstance().setSelectedBundle(medias);
        return mPickerFragment;
    }

    public void createPageInContainer(int container, int position) {
        FractionScheduler fractionScheduler = mFractionManager.startFractionScheduler();
        if (mCurFraction != null) {
            fractionScheduler.hide(mCurFraction);
        }
        String tag = container + ":" + position;
        Fraction fraction;
        Optional<Fraction> fractionOptional = mFractionManager.getFractionByTag(tag);
        if (fractionOptional.isPresent()) {
            fraction = fractionOptional.get();
            fractionScheduler.show(fraction);
        } else {
            fraction = getPage(position);
            fractionScheduler.add(container, fraction, tag);
        }
        mCurFraction = fraction;
        fractionScheduler.submit();
    }

    public Fraction getPage(int position) {
        Optional<Fraction> fractionOptional = mFractionPlainArray.get(position);
        if (fractionOptional.isPresent()) {
            return fractionOptional.get();
        }
        Fraction fraction = mFractions.get(position);
        mFractionPlainArray.put(position, fraction);
        return fraction;
    }

    private void setTitleTxt(BoxingConfig config) {
        Image backImage = (Image) findComponentById(ResourceTable.Id_backImage);
        Text titleTxt = (Text) findComponentById(ResourceTable.Id_titleText);
        backImage.setClickedListener(component -> terminateAbility());
        if (config.getMode() == BoxingConfig.Mode.VIDEO) {
            titleTxt.setText("视频相册");
            titleTxt.setAroundElements(null, null, null, null);
            return;
        }
        mPickerFragment.setTitleTxt(titleTxt);
    }

    @Override
    public void onBoxingFinish(Intent intent, List<BaseMedia> medias) {
        String[] str = new String[medias.size()];
        for (int i = 0; i < medias.size(); i++) {
            str[i] = medias.get(i).getId();
        }
        Intent intent1 = new Intent();
        intent1.setParam(Boxing.EXTRA_RESULT_ID, str);
        setResult(Boxing.COMPRESS_RESULT_CODE, intent1);
        terminateAbility();
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (null == resultData) return;
        switch (resultCode) {
            case Boxing.CAMERA_RESULT_CODE: //Camera Photo Return
                File file = resultData.getSerializableParam(Boxing.EXTRA_RESULT_CAMERA);
                Intent intent = new Intent();
                intent.setParam(Boxing.EXTRA_RESULT_CAMERA, file);
                setResult(Boxing.CAMERA_RESULT_CODE, intent);
                terminateAbility();
                break;
            case Boxing.COMPRESS_RESULT_CODE: //Picture Multiple Preview OK button returns
                if (!resultData.getBooleanParam(EXTRA_TYPE_BACK, false)) {
                    boolean back = resultData.getBooleanParam(EXTRA_TYPE_BACK, false);
                    String[] str = resultData.getStringArrayParam(Boxing.EXTRA_RESULT_ID);
                    Intent intent1 = new Intent();
                    intent1.setParam(EXTRA_TYPE_BACK, back);
                    intent1.setParam(Boxing.EXTRA_RESULT_ID, str);
                    setResult(Boxing.COMPRESS_RESULT_CODE, intent1);
                    terminateAbility();
                }
                mPickerFragment.mIsPreview = false;
                break;
            case Boxing.CROP_RESULT_CODE:
                Intent intent1 = new Intent();
                setResult(Boxing.CROP_RESULT_CODE2, intent1);
                terminateAbility();
                break;
        }
    }
}
