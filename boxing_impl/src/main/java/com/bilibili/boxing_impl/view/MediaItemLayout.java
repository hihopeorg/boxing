/*
 *  Copyright (C) 2017 Bilibili
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.bilibili.boxing_impl.view;

import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing.model.entity.impl.ImageMedia;
import com.bilibili.boxing.model.entity.impl.VideoMedia;
import com.bilibili.boxing_impl.BoxingResHelper;
import com.bilibili.boxing_impl.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.media.photokit.metadata.AVThumbnailUtils;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;


/**
 * A media layout for {@link ohos.agp.components.ListContainer} item, including image and video <br/>
 */
public class MediaItemLayout extends StackLayout {
    private static final int BIG_IMG_SIZE = 5 * 1024 * 1024;

    private Image mCheckImg;
    private Component mVideoLayout;
    private Component mFontLayout;
    private Image mCoverImg;
    private ScreenType mScreenType;

    private enum ScreenType {
        SMALL(100), NORMAL(180), LARGE(320);
        int value;

        ScreenType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public MediaItemLayout(Context context) {
        super(context);
    }

    public MediaItemLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context);
    }

    public MediaItemLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        Component view = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_boxing_media_item, null, false);
        mCoverImg = (Image) view.findComponentById(ResourceTable.Id_media_item);
        mCheckImg = (Image) view.findComponentById(ResourceTable.Id_media_item_check);
        mVideoLayout = view.findComponentById(ResourceTable.Id_video_layout);
        mFontLayout = view.findComponentById(ResourceTable.Id_media_font_layout);
        mScreenType = getScreenType(context);
        addComponent(view);
    }

    private ScreenType getScreenType(Context context) {
        return ScreenType.NORMAL;
    }

    public void setImageRes(int imageRes) {
        if (mCoverImg != null) {
            mCoverImg.setPixelMap(imageRes);
        }
    }

    public void setMedia(Ability ability, BaseMedia media) {
        if (media instanceof ImageMedia) {
            mVideoLayout.setVisibility(Component.HIDE);
            DataAbilityHelper helper = DataAbilityHelper.creator(ability);
            ImageMedia imageMedia = (ImageMedia) media;
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(imageMedia.getId()));
            try {
                FileDescriptor fd = helper.openFile(uri, "r");
                ImageSource imageSource = ImageSource.create(fd, null);
                PixelMap pixelMap = imageSource.createPixelmap(null);
                mCoverImg.setPixelMap(pixelMap);
            } catch (DataAbilityRemoteException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else if (media instanceof VideoMedia) {
            mVideoLayout.setVisibility(VISIBLE);
            VideoMedia videoMedia = (VideoMedia) media;
            Text durationTxt = ((Text) mVideoLayout.findComponentById(ResourceTable.Id_video_duration_txt));
            durationTxt.setText(videoMedia.getDuration());
            ((Text) mVideoLayout.findComponentById(ResourceTable.Id_video_size_txt)).setText(videoMedia.getSizeByUnit());

            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(videoMedia.getId()));
            setCover(videoMedia.getPath());
        }
    }

    private void setCover(String path) {
        if (mCoverImg == null || path.isEmpty()) {
            return;
        }
        //通过视频路径获取视频缩略图
        Size size = new Size(300, 300);
        PixelMap resMap = AVThumbnailUtils.createVideoThumbnail(new File(path), size);
        mCoverImg.setPixelMap(resMap);
    }

    @SuppressWarnings("deprecation")
    public void setChecked(boolean isChecked) {
        if (isChecked) {
            mFontLayout.setVisibility(Component.VISIBLE);
            mCheckImg.setPixelMap(BoxingResHelper.getMediaCheckedRes());
        } else {
            mFontLayout.setVisibility(Component.HIDE);
            mCheckImg.setPixelMap(BoxingResHelper.getMediaUncheckedRes());
        }
    }

}
