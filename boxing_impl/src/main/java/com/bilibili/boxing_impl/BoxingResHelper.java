package com.bilibili.boxing_impl;


import com.bilibili.boxing.model.BoxingManager;

/**
 * Help getting the resource in config.
 */

public class BoxingResHelper {

    public static int getMediaCheckedRes() {
        int result = BoxingManager.getInstance().getBoxingConfig().getMediaCheckedRes();
        return result > 0 ? result : ResourceTable.Media_ic_boxing_checked;
    }

    public static int getMediaUncheckedRes() {
        int result = BoxingManager.getInstance().getBoxingConfig().getMediaUnCheckedRes();
        return result > 0 ? result : ResourceTable.Media_ic_boxing_uncheck;
    }

    public static int getCameraRes() {
        return BoxingManager.getInstance().getBoxingConfig().getCameraRes();
    }
}
