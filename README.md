# boxing

**本项目是基于开源项目boxing进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/bilibili/boxing ）追踪到原项目版本**

#### 项目介绍

- 项目名称：基于MVP模式的多媒体选择器
- 所属系列：ohos的第三方组件适配移植
- 功能：支持多/单图片选择和预览，单图裁剪功能；支持视频选择功能、视频播放功能；支持自定义UI
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/bilibili/boxing
- 原项目基线版本：v1.0.4 , sha1:bd1eeff8405a74c201ca489d6f753e306e84d195
- 编程语言：Java 
- 外部库依赖：无

#### 效果展示

<img src="preview/preview.gif"/>

#### 安装教程

方法1.

1. 编译har包boxing.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

1. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.bilibili.ohos:boxing:1.0.1'
    implementation 'com.bilibili.ohos:boxing-impl:1.0.1'
    implementation 'com.bilibili.ohos:ucrop:1.0.1'
}
```

#### 使用说明

1. 添加权限
 "reqPermissions": [
       {
         "name": "ohos.permission.MICROPHONE"
       },
       {
         "name": "ohos.permission.LOCATION"
       },
       {
         "name": "ohos.permission.CAMERA"
       },
       {
         "name": "ohos.permission.WRITE_MEDIA"
       },
       {
         "name": "ohos.permission.READ_MEDIA"
       }
     ]

2. 简单用法
 初始化图片加载（必选）
 ```java
 BoxingMediaLoader.getInstance().init(new IBoxingMediaLoader()); // 需要实现IBoxingMediaLoader
 ```

 初始化图片裁剪（可选）
 ```java
 BoxingCrop.getInstance().init(new IBoxingCrop());  // 需要实现 IBoxingCrop
 ```

 构造参数 指定模式：图片单选，多选，视频单选，是否支持gif和相机
 ```java
 BoxingConfig config = new BoxingConfig(Mode); // Mode：Mode.SINGLE_IMG, Mode.MULTI_IMG, Mode.VIDEO
 config.needCamera(cameraRes).needGif().withMaxCount(9); // 支持gif，相机，设置最大选图数
 .withMediaPlaceHolderRes(resInt) // 设置默认图片占位图，默认无
 .withAlbumPlaceHolderRes(resInt) // 设置默认相册占位图，默认无
 .withVideoDurationRes(resInt) // 视频模式下，时长的图标，默认无
 ```

 初始化Boxing，构造Intent并启动
 ```java
 // 启动缩略图界面, 依赖boxing-impl.
 Boxing.of(config).withIntent(context, BoxingActivity.class).start(callerActivity, REQUEST_CODE);
 // 启动预览原图界面，依赖boxing-impl.
 Boxing.of(config).withIntent(context, BoxingViewActivity.class).start(callerActivity, REQUEST_CODE);
 // 调用of方法默认使用Mode.MULTI_IMG
 Boxing.of().withIntent(context, class).start(callerActivity, REQUEST_CODE);
 ```

 取结果
 ```java
 @Override
     protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
         super.onAbilityResult(requestCode, resultCode, resultData);
         String[] str = resultData.getStringArrayParam(Boxing.EXTRA_RESULT_ID);
 }
 ```

3. 进阶用法
 初始化图片加载和裁剪同上
  自定义Ability与Fraction 继承AbsBoxingViewActivity和AbsBoxingViewFragment。 调用Boxing.of(config).withIntent(context, class).start(callerActivity, REQUEST_CODE);启动。
  仅自定义Fraction 继承AbsBoxingViewFragment，不依赖AbsBoxingViewActivity。 调用Boxing.of(BoxingConfig).setupFragment(AbsBoxingViewFragment, OnFinishListener);完成配置


#### 版本迭代
- v1.0.1

#### 版权和许可信息
- Apache Licence

