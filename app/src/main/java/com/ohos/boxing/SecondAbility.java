package com.ohos.boxing;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.ResultMedia;
import com.bilibili.boxing_impl.ui.BoxingBottomSheetActivity;
import com.bilibili.boxing_impl.ui.BoxingViewActivity;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Image;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import static com.bilibili.boxing.Boxing.EXTRA_SELECTED_MEDIA;

public class SecondAbility extends FractionAbility {

    private Image mResultImage;

    private String mMediaId;
    private final int PERMISSIONS_REQUEST_WRITE = 102;
    private static final int COMPRESS_REQUEST_CODE = 2048;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_second);
        checkPermissionAndLoad();
        mResultImage = (Image) findComponentById(ResourceTable.Id_resultImage);
        findComponentById(ResourceTable.Id_outside_bs_btn).setClickedListener(component -> {
            BoxingConfig singleImgConfig = new BoxingConfig(BoxingConfig.Mode.SINGLE_IMG).withMediaPlaceHolderRes(ResourceTable.Media_ic_boxing_default_image);
            Boxing.of(singleImgConfig).withIntent(this, BoxingBottomSheetActivity.class).start(this, COMPRESS_REQUEST_CODE, getBundleName(), BoxingBottomSheetActivity.class.getName());
        });
        mResultImage.setClickedListener(component -> {
            if (null == mMediaId || mMediaId.isEmpty()) return;
            ArrayList<ResultMedia> medias = new ArrayList<>();
            ResultMedia imageMedia = new ResultMedia();
            imageMedia.setId(mMediaId);
            medias.add(imageMedia);
            Intent intent1 = new Intent();
            intent1.setSequenceableArrayListParam(EXTRA_SELECTED_MEDIA, medias);
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(BoxingViewActivity.class.getName())
                    .build();
            intent1.setOperation(operation);
            startAbility(intent1);
        });
    }

    String[] permissions = {"ohos.permission.CAMERA"
            , "ohos.permission.MICROPHONE"
            , "ohos.permission.LOCATION"
            , "ohos.permission.WRITE_MEDIA"
            , "ohos.permission.READ_MEDIA"};

    private void checkPermissionAndLoad() {
        if (canRequestPermission("ohos.permission.READ_MEDIA")) {
            requestPermissionsFromUser(permissions, PERMISSIONS_REQUEST_WRITE);
        } else {

        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultData == null) return;
        if (resultCode == Boxing.COMPRESS_RESULT_CODE) {
            switch (requestCode) {
                case COMPRESS_REQUEST_CODE: //Photo radio return
                    String[] str = resultData.getStringArrayParam(Boxing.EXTRA_RESULT_ID);
                    mMediaId = str[0];
                    DataAbilityHelper helper = DataAbilityHelper.creator(this);
                    Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, str[0]);
                    try {
                        FileDescriptor fd = helper.openFile(uri, "r");
                        ImageSource imageSource = ImageSource.create(fd, null);
                        PixelMap pixelMap = imageSource.createPixelmap(null);
                        mResultImage.setPixelMap(pixelMap);
                    } catch (DataAbilityRemoteException e) {
                        e.printStackTrace();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
            }
            return;
        }
    }
}
