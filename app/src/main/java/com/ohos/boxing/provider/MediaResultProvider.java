/*
 *  Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.ohos.boxing.provider;

import com.bilibili.boxing_impl.ResourceTable;
import com.bilibili.boxing_impl.WindowManagerHelper;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class MediaResultProvider extends BaseItemProvider {

    private List<String> mMedias;
    private Component.ClickedListener mOnCameraClickListener;
    private Component.ClickedListener mOnMediaClickListener;
    private OnMediaCheckedListener mOnCheckedListener;
    private int mDefaultRes;

    private Ability mAbility;
    private final int mColumnNum = 3;
    private final int mItemMargin = 6;
    private final int mItemMarginWidth = 20;

    public MediaResultProvider(Ability context) {
        this.mAbility = context;
        this.mMedias = new ArrayList<>();
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {

        ImageViewHolder imageViewHolder = null;
        if (component == null) {
            component = LayoutScatter.getInstance(mAbility).parse(ResourceTable.Layout_layout_boxing_result_item, null, false);
            imageViewHolder = new ImageViewHolder();
            imageViewHolder.image = (Image) component.findComponentById(ResourceTable.Id_image);
            imageViewHolder.parentV = (DirectionalLayout) component.findComponentById(ResourceTable.Id_parentV);
            component.setTag(imageViewHolder);
        } else {
            imageViewHolder = (ImageViewHolder) component.getTag();
        }
        final String id = mMedias.get(position);
        DataAbilityHelper helper = DataAbilityHelper.creator(mAbility);
        Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, id);
        try {
            FileDescriptor fd = helper.openFile(uri, "r");
            ImageSource imageSource = ImageSource.create(fd, null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            imageViewHolder.image.setPixelMap(pixelMap);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int screenHeight = WindowManagerHelper.getScreenHeight(mAbility.getContext());
        int screenWidth = WindowManagerHelper.getScreenWidth(mAbility.getContext());
        ComponentContainer.LayoutConfig layoutConfig = imageViewHolder.image.getLayoutConfig();
        layoutConfig.width = screenWidth / mColumnNum - mItemMarginWidth;
        layoutConfig.height = screenWidth / mColumnNum - mItemMarginWidth;
        layoutConfig.setMargins(mItemMargin, mItemMargin, mItemMargin, mItemMargin);
        imageViewHolder.image.setLayoutConfig(layoutConfig);
        imageViewHolder.parentV.setAlignment(LayoutAlignment.CENTER);

        return component;
    }

    @Override
    public Object getItem(int i) {
        return mMedias.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getCount() {
        return mMedias.size();
    }

    //为了优化LisContainer不可避免的使用了ViewHolder来复用
    private class ImageViewHolder {
        DirectionalLayout parentV;
        Image image;
    }

    public void setOnCameraClickListener(Component.ClickedListener onCameraClickListener) {
        mOnCameraClickListener = onCameraClickListener;
    }

    public void setOnCheckedListener(OnMediaCheckedListener onCheckedListener) {
        mOnCheckedListener = onCheckedListener;
    }

    public void setOnMediaClickListener(Component.ClickedListener onMediaClickListener) {
        mOnMediaClickListener = onMediaClickListener;
    }

    public void setSelectedMedias(List<String> selectedMedias) {
        if (selectedMedias == null) {
            return;
        }
        notifyDataChanged();
    }

    public void addAllData(List<String> data) {
        int oldSize = mMedias.size();
        this.mMedias.addAll(data);
        int size = data.size();
        notifyDataSetItemRangeInserted(oldSize, size);
    }

    public void clearData() {
        int size = mMedias.size();
        this.mMedias.clear();
        notifyDataSetItemRangeRemoved(0, size);
    }

    public List<String> getAllMedias() {
        return mMedias;
    }

    public interface OnMediaCheckedListener {
        /**
         * In multi image mode, selecting a {@link String} or undo.
         */
        void onChecked(Component v, String iMedia);
    }

}
