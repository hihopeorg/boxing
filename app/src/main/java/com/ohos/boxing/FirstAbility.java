package com.ohos.boxing;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.VideoAbility;
import com.bilibili.boxing.model.BoxingManager;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.config.BoxingCropOption;
import com.bilibili.boxing.utils.BoxingFileHelper;
import com.bilibili.boxing_impl.WindowManagerHelper;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.bilibili.boxing_impl.ui.BoxingBehaviorActivity;
import com.ohos.boxing.provider.MediaResultProvider;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Locale;


public class FirstAbility extends Ability implements Component.ClickedListener {

    private ListContainer mListContainer;
    private Image mResultImage;
    private MediaResultProvider mMediaResultProvider;

    private final int mColumnNum = 3;
    private final int mItemMarginWidth = 20;

    private final int PERMISSIONS_REQUEST_CAMERA = 100;
    private final int PERMISSIONS_REQUEST_WRITE = 102;

    private static final int REQUEST_CODE = 1024;
    private static final int COMPRESS_REQUEST_CODE = 2048;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_first);
        checkPermissionAndLoad();
        initListContainer();
        initListener();
    }

    private void initListContainer() {
        mResultImage = (Image) findComponentById(ResourceTable.Id_resultImage);
        mListContainer = (ListContainer) findComponentById(ResourceTable.Id_resultListContainer);
        mMediaResultProvider = new MediaResultProvider(this);
        TableLayoutManager manager = new TableLayoutManager();
        manager.setColumnCount(3);
        mListContainer.setLayoutManager(manager);
        mListContainer.setItemProvider(mMediaResultProvider);
        mListContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                BoxingConfig.Mode mode = BoxingManager.getInstance().getBoxingConfig().getMode();
                if (mode != BoxingConfig.Mode.VIDEO) return;
                Intent intent = new Intent();
                intent.setParam(Boxing.EXTRA_RESULT_VIDEO_ID, mMediaResultProvider.getItem(i).toString());
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName(VideoAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    private void initListener() {
        findComponentById(ResourceTable.Id_single_image_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_single_image_btn_crop_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_multi_image_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_video_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_outside_bs_btn).setClickedListener(this::onClick);
    }

    String[] permissions = {"ohos.permission.CAMERA"
            , "ohos.permission.MICROPHONE"
            , "ohos.permission.LOCATION"
            , "ohos.permission.WRITE_MEDIA"
            , "ohos.permission.READ_MEDIA"};

    private void checkPermissionAndLoad() {
        if (canRequestPermission("ohos.permission.READ_MEDIA")) {
            requestPermissionsFromUser(permissions, PERMISSIONS_REQUEST_WRITE);
        } else {

        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_single_image_btn:
                BoxingConfig singleImgConfig = new BoxingConfig(BoxingConfig.Mode.SINGLE_IMG).withMediaPlaceHolderRes(ResourceTable.Media_ic_boxing_default_image);
                Boxing.of(singleImgConfig).withIntent(this, BoxingActivity.class).start(this, COMPRESS_REQUEST_CODE, getBundleName(), BoxingActivity.class.getName());
                break;
            case ResourceTable.Id_single_image_btn_crop_btn:
                String cachePath = BoxingFileHelper.getCacheDir(this);
                if (cachePath.isEmpty()) {
                    new ToastDialog(this).setText("设备存储读取出错或暂不可用，请稍候重试").show();
                    return;
                }
                Uri destUri = new Uri.Builder()
                        .scheme("file")
                        .appendDecodedPath(cachePath)
                        .appendDecodedPath(String.format(Locale.US, "%s.png", System.currentTimeMillis()))
                        .build();
                BoxingCropOption cropOption = new BoxingCropOption(destUri);
                BoxingConfig singleCropImgConfig = new BoxingConfig(BoxingConfig.Mode.SINGLE_IMG).withCropOption(cropOption)
                        .withMediaPlaceHolderRes(ResourceTable.Media_ic_boxing_default_image);
                Boxing.of(singleCropImgConfig).withIntent(this, BoxingActivity.class).start(this, REQUEST_CODE, getBundleName(), BoxingActivity.class.getName());
                break;
            case ResourceTable.Id_multi_image_btn:
                checkCameraPermissionAndLoad();
                break;
            case ResourceTable.Id_video_btn:
                BoxingConfig videoConfig = new BoxingConfig(BoxingConfig.Mode.VIDEO).withVideoDurationRes(ResourceTable.Media_ic_boxing_play);
                Boxing.of(videoConfig).withIntent(this, BoxingActivity.class).start(this, REQUEST_CODE, getBundleName(), BoxingActivity.class.getName());
                break;
            case ResourceTable.Id_outside_bs_btn:
                BoxingConfig behaviorConfig = new BoxingConfig(BoxingConfig.Mode.SINGLE_IMG).withMediaPlaceHolderRes(ResourceTable.Media_ic_boxing_default_image);
                Boxing.of(behaviorConfig).withIntent(this, BoxingBehaviorActivity.class).start(this, 1, getBundleName(), BoxingBehaviorActivity.class.getName());
                break;
        }
    }

    private void checkCameraPermissionAndLoad() {
        if (verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED) {
            if (canRequestPermission("ohos.permission.CAMERA")) {
                requestPermissionsFromUser(
                        new String[]{"ohos.permission.CAMERA"}, PERMISSIONS_REQUEST_CAMERA);
            } else {

            }
        } else {
            startMultiImageAbility();
        }
    }

    private void startMultiImageAbility() {
        BoxingConfig multiImgConfig = new BoxingConfig(BoxingConfig.Mode.MULTI_IMG).needCamera(ResourceTable.Media_ic_boxing_camera_white).needGif();
        Boxing.of(multiImgConfig).withIntent(this, BoxingActivity.class).start(this, REQUEST_CODE, getBundleName(), BoxingActivity.class.getName());
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    startMultiImageAbility();
                } else {

                }
                return;
            }
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultData == null) return;
        if (resultCode == Boxing.COMPRESS_RESULT_CODE) {
            switch (requestCode) {
                case COMPRESS_REQUEST_CODE: //Photo radio return
                case REQUEST_CODE: //Image multi-select return, video select return
                    mResultImage.setVisibility(Component.HIDE);
                    mListContainer.setVisibility(Component.VISIBLE);
                    mMediaResultProvider.clearData();
                    String[] str = resultData.getStringArrayParam(Boxing.EXTRA_RESULT_ID);
                    mMediaResultProvider.addAllData(Arrays.asList(str));
                    break;
            }
            return;
        }
        if (resultCode == Boxing.CAMERA_RESULT_CODE) { //Camera Photo Return
            mResultImage.setVisibility(Component.VISIBLE);
            mListContainer.setVisibility(Component.HIDE);
            File file = resultData.getSerializableParam(Boxing.EXTRA_RESULT_CAMERA);
            ImageSource.DecodingOptions checkOptions = new ImageSource.DecodingOptions();
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            ImageSource imageSource = ImageSource.create(file, srcOpts);
            PixelMap pixelMap = imageSource.createPixelmap(checkOptions);

            int screenWidth = WindowManagerHelper.getScreenWidth(this);
            ComponentContainer.LayoutConfig layoutConfig = mResultImage.getLayoutConfig();
            layoutConfig.width = screenWidth / mColumnNum - mItemMarginWidth;
            layoutConfig.height = screenWidth / mColumnNum - mItemMarginWidth;
            layoutConfig.setMargins(40, 35, 0, 0);
            mResultImage.setLayoutConfig(layoutConfig);

            mResultImage.setPixelMap(pixelMap);
            return;
        }
        if (resultCode == Boxing.CROP_RESULT_CODE2) { //Cutback
            DataAbilityHelper helper = DataAbilityHelper.creator(this);
            try {
                ResultSet resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI,
                        new String[]{AVStorage.Images.Media.ID}, null);
                while (resultSet != null && resultSet.goToNextRow()) {
                    PixelMap pixelMap = null;
                    ImageSource imageSource = null;
                    int id = resultSet.getInt(resultSet.getColumnIndexForName(AVStorage.Images.Media.ID));
                    Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
                    FileDescriptor fd = helper.openFile(uri, "r");
                    try {
                        imageSource = ImageSource.create(fd, null);
                        pixelMap = imageSource.createPixelmap(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (imageSource != null) {
                            imageSource.release();
                        }
                    }
                    int screenWidth = WindowManagerHelper.getScreenWidth(this);
                    ComponentContainer.LayoutConfig layoutConfig = mResultImage.getLayoutConfig();
                    layoutConfig.width = screenWidth / mColumnNum - mItemMarginWidth;
                    layoutConfig.height = screenWidth / mColumnNum - mItemMarginWidth;
                    layoutConfig.setMargins(40, 35, 0, 0);
                    mResultImage.setLayoutConfig(layoutConfig);
                    mResultImage.setPixelMap(pixelMap);
                    mResultImage.setVisibility(Component.VISIBLE);
                    mListContainer.setVisibility(Component.HIDE);
                }
            } catch (DataAbilityRemoteException | FileNotFoundException e) {
                e.printStackTrace();
            }
            return;
        }
    }
}
