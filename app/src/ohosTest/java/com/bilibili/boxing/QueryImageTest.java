/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bilibili.boxing;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import org.junit.Assert;
import org.junit.Test;

public class QueryImageTest {

    @Test
    public void testBuildImage() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = sAbilityDelegator.getAppContext();
        DataAbilityHelper abilityHelper = DataAbilityHelper.creator(context);
        ResultSet cur = null;
        try {
            try {
                cur = abilityHelper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID, AVStorage.Images.Media.DATA}, null);
            } catch (DataAbilityRemoteException e) {
                e.printStackTrace();
            }
            if (cur != null && cur.goToFirstRow()) {
                do {
                    cur.getString(cur.getColumnIndexForName(AVStorage.Images.Media.ID));
                    cur.getString(cur.getColumnIndexForName(AVStorage.Images.Media.DATA));
                } while (cur.goToNextRow() && !cur.isAtLastRow());
            }
            Assert.assertTrue(cur.getColumnCount() > 0);
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

}