/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.boxing;

import com.bilibili.boxing.AbsBoxingActivity;
import com.bilibili.boxing.CameraAbility;
import com.bilibili.boxing.VideoAbility;
import com.bilibili.boxing.camera.CameraInfoConfig;
import com.bilibili.boxing_impl.ui.*;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.*;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.app.Context;
import ohos.global.resource.RawFileDescriptor;
import ohos.media.common.Source;
import ohos.media.player.Player;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.Assert;
import org.junit.Test;

public class ExampleOhosTest extends TestCase {

    private IAbilityDelegator mIAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;
    private AbsBoxingActivity mAbility;
    private BoxingViewFragment mFraction;
    private ListContainer mListContainer;

    @Override
    protected void setUp() throws Exception {
        mContext = mIAbilityDelegator.getAppContext();
        mAbility = EventHelper.startAbility(BoxingActivity.class);
        EventHelper.waitForActive(mAbility, 5);
        Thread.sleep(3000L);
        mFraction = (BoxingViewFragment) mAbility.getFractionManager().getFractionByTag(ResourceTable.Id_stackLayout + ":" + 0).get();
        mListContainer = (ListContainer) mFraction.getFractionAbility().findComponentById(ResourceTable.Id_listContainer);
    }

    @Override
    protected void tearDown() throws Exception {
        Thread.sleep(2000);
        mIAbilityDelegator.stopAbility(mAbility);
        EventHelper.clearAbilities();
    }

    private void simulateClickOnScreen(Component component) {
        int[] pos = component.getLocationOnScreen();
        pos[0] += component.getWidth() / 2;
        pos[1] += component.getHeight() / 2;

        simulateClickOnScreen(pos[0], pos[1]);
    }

    private void simulateClickOnScreen(int x, int y) {
        long downTime = Time.getRealActiveTime();
        long upTime = downTime;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, x, y);
        mIAbilityDelegator.triggerTouchEvent(mAbility, ev1);
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, x, y);
        mIAbilityDelegator.triggerTouchEvent(mAbility, ev2);
    }

    @Test
    public void testSinglePhoto() throws InterruptedException {
        Component item = mListContainer.getComponentAt(0);
        simulateClickOnScreen(item);
        Thread.sleep(2000L);
        Assert.assertTrue(mFraction.getMediaAdapter().getSelectedMedias().size() == 1);
    }

    @Test
    public void testPhotoAlbum() throws InterruptedException {
        Text albumText = (Text) mAbility.findComponentById(ResourceTable.Id_titleText);
        simulateClickOnScreen(albumText);
        Thread.sleep(2000L);
        Assert.assertTrue(mFraction.getAlbumAdapter().getCount() > 1);
    }

    @Test
    public void testMorePhoto() throws InterruptedException {
        Component item = mListContainer.getComponentAt(0);
        Image mCheckImg = (Image) item.findComponentById(ResourceTable.Id_media_item_check);
        simulateClickOnScreen(mCheckImg);
        Thread.sleep(2000L);
        Component item1 = mListContainer.getComponentAt(1);
        Image mCheckImg1 = (Image) item1.findComponentById(ResourceTable.Id_media_item_check);
        simulateClickOnScreen(mCheckImg1);
        Thread.sleep(2000L);
        Assert.assertTrue(mFraction.getMediaAdapter().getSelectedMedias().size() == 2);
    }

    @Test
    public void testMoreSelectAndPreview() throws InterruptedException {
        Component item = mListContainer.getComponentAt(0);
        Image mCheckImg = (Image) item.findComponentById(ResourceTable.Id_media_item_check);
        simulateClickOnScreen(mCheckImg);
        Thread.sleep(2000L);
        Component item1 = mListContainer.getComponentAt(1);
        Image mCheckImg1 = (Image) item1.findComponentById(ResourceTable.Id_media_item_check);
        simulateClickOnScreen(mCheckImg1);
        Thread.sleep(2000L);
        Button button = (Button) mFraction.getFractionAbility().findComponentById(ResourceTable.Id_choose_preview_btn);
        EventHelper.triggerClickEvent(mAbility, button);
        Thread.sleep(2000L);
        Assert.assertTrue(mFraction.getMediaAdapter().getSelectedMedias().size() == 2);
    }

    @Test
    public void testSinglePhotoAndTailor() throws InterruptedException {
        Component item = mListContainer.getComponentAt(1);
        simulateClickOnScreen(item);
        Thread.sleep(2000L);
        Assert.assertTrue(mFraction.getMediaAdapter().getSelectedMedias().size() == 1);
    }

    @Test
    public void testTakePicture() throws InterruptedException {
        CameraAbility ability = EventHelper.startAbility(CameraAbility.class);
        EventHelper.waitForActive(ability, 5);
        Button button = (Button) ability.findComponentById(ResourceTable.Id_picture_button);
        EventHelper.triggerClickEvent(ability, button);
        Thread.sleep(3000L);
        Assert.assertTrue(CameraInfoConfig.cameraSize.width > 0);
    }

    @Test
    public void testSingleVideo() throws InterruptedException {
        Component item = mListContainer.getComponentAt(0);
        simulateClickOnScreen(item);
        Thread.sleep(2000L);
        Assert.assertTrue(mFraction.getMediaAdapter().getSelectedMedias().size() == 1);
    }

    private Player mPlayer;
    private SurfaceProvider mSurfaceProvider;
    Object mLock = new Object();
    final boolean[] isFinish = new boolean[1];

    @Test
    public void testVideoPlay() {
        VideoAbility ability = EventHelper.startAbility(VideoAbility.class);
        EventHelper.waitForActive(mAbility, 5);
        mSurfaceProvider = (SurfaceProvider) ability.findComponentById(com.bilibili.boxing.ResourceTable.Id_surfaceProvider);
        mPlayer = new Player(mContext);
        mSurfaceProvider.getSurfaceOps().get().addCallback(new VideoSurfaceCallback());
        mSurfaceProvider.pinToZTop(true);

        synchronized (mLock) {
            isFinish[0] = false;
            while (isFinish[0] != true) {
                try {
                    mLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        Assert.assertTrue(mPlayer.isNowPlaying());
    }

    class VideoSurfaceCallback implements SurfaceOps.Callback {

        @Override
        public void surfaceCreated(SurfaceOps surfaceOps) {

            if (mSurfaceProvider.getSurfaceOps().isPresent()) {
                Surface surface = mSurfaceProvider.getSurfaceOps().get().getSurface();
                try {
                    RawFileDescriptor filDescriptor = mContext.getResourceManager().getRawFileEntry("resources/rawfile/abc.mp4").openRawFileDescriptor();
                    Source source = new Source(filDescriptor.getFileDescriptor(), filDescriptor.getStartPosition(), filDescriptor.getFileSize());
                    mPlayer.setSource(source);
                    mPlayer.setVideoSurface(surface);
                    mPlayer.setPlayerCallback(new VideoPlayerCallback());
                    mPlayer.prepare();
                    mSurfaceProvider.setTop(0);
                    mPlayer.play();

                    isFinish[0] = true;
                    synchronized (mLock) {
                        mLock.notifyAll();
                    }
                } catch (Exception e) {
                }

            }
        }

        @Override
        public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {
            isFinish[0] = true;
            synchronized (mLock) {
                mLock.notifyAll();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceOps surfaceOps) {
            isFinish[0] = true;
            synchronized (mLock) {
                mLock.notifyAll();
            }
        }

    }

    public class VideoPlayerCallback implements Player.IPlayerCallback {

        @Override
        public void onPrepared() {
        }

        @Override
        public void onMessage(int i, int i1) {

        }

        @Override
        public void onError(int i, int i1) {

        }

        @Override
        public void onResolutionChanged(int i, int i1) {

        }

        @Override
        public void onPlayBackComplete() {

            if (mPlayer != null) {
                mPlayer.play();
                isFinish[0] = true;
                synchronized (mLock) {
                    mLock.notifyAll();
                }
            }
        }

        @Override
        public void onRewindToComplete() {

        }

        @Override
        public void onBufferingChange(int i) {

        }

        @Override
        public void onNewTimedMetaData(Player.MediaTimedMetaData mediaTimedMetaData) {
        }

        @Override
        public void onMediaTimeIncontinuity(Player.MediaTimeInfo mediaTimeInfo) {
        }
    }

    @Test
    public void testFractionPhoto() throws InterruptedException {
        BoxingBehaviorActivity ability = EventHelper.startAbility(BoxingBehaviorActivity.class);
        EventHelper.waitForActive(ability, 5);
        Thread.sleep(2000L);
        BoxingBehaviorFragment fraction = (BoxingBehaviorFragment) mAbility.getFractionManager().getFractionByTag(ResourceTable.Id_stackLayout + ":" + 0).get();
        ListContainer listContainer = (ListContainer) fraction.getFractionAbility().findComponentById(ResourceTable.Id_listContainer);
        simulateClickOnScreen(listContainer.getComponentAt(0));
        Thread.sleep(2000L);
        Image resultImage = (Image) ability.findComponentById(ResourceTable.Id_resultImage);
        Assert.assertTrue(resultImage.getPixelMap() != null);
    }

    @Test
    public void testCustomFractionPhoto() throws InterruptedException {
        BoxingBottomSheetActivity ability = EventHelper.startAbility(BoxingBottomSheetActivity.class);
        EventHelper.waitForActive(ability, 5);
        Thread.sleep(2000L);
        BoxingBottomSheetFragment fraction = (BoxingBottomSheetFragment) mAbility.getFractionManager().getFractionByTag(ResourceTable.Id_stackLayout + ":" + 0).get();
        Assert.assertTrue(fraction.getMediaAdapter().getCount() > 0);
    }

}